# MyCRUD

**[Under development, not ready yet!]**

MyCRUD is a simple web-enabled key-value store and messaging with the following features:

- Can be used directly from web/apps via HTTP-API with CORS-support and token-based authentication. *No backend code needed*.
- Timestamped data allows: atomic updates, incremental backup, *offline apps*, values with limited time-to-live, *pub-sub communication*, etc.
- **TODO:** Docker image, deployable to Kubernetes. Add HTTP/2 reverse proxy for best performance.
- **TODO:** WordPress plugin, *installable by less-technical users*.

It can be used directly by web/apps, – and thus allow the user to host their own data, which brings the following benefits:

- Scalability – the users can bring as much storage or bandwidth as they need.
- Privacy – the users data are on the users own server – easier GDPR compliance.
- Less need for content filters – the users are liable and responsible for their own content – easier Article 13 compliance.
- Longivity – the web/app can continue to be used, even if no longer supported by the developer.

## API

The HTTP-API supports CORS, and tokens are passed with the `Authorization: Bearer $TOKEN` header.

- `GET /$DB/$KEY` returns the value
    - Version/timestamp is in ETag header
- `PUT /$DB/$KEY[?prevVersion=...][&forceVersion=...]` saves a value and returns the new version-timestamp
    - `prevVersion` parameter allows atomic updates with compare-and-set semantic
    - `forceVersion` is used to write a custom version instead of current timestamp, – only to be used to restore backups.
- `DELETE /$DB/$KEY[?prevVersion=...][&forceVersion=...]`
    - `prevVersion` parameter allows atomic updates
    - `forceVersion` is used to write a custom version instead of current timestamp, – only to be used to restore backups.
- `GET /$DB[?gt=...][&gte=...][&lt=...][&lte=...][&since=...][&wait=...][&reverse=true][&limit=...][&deleted=true]` finds a list of `[[$key, $version], ...]`. Query is designed to be compatible with leveldown.
    - `since` find keys whoose value has been put or deleted since a given timestamp
    - `gt`, `gte`, `lt`, `lte` find keys greater, less-than or equal to these values. Cannot be combined with `since`
    - `wait` – if the result is empty, then wait the given number of seconds for new data that matches the query
    - `limit` – maximum number of results, at most 10.000, defaults to 100
    - `reverse` – get result in descending order. Defaults to false.
    - `deleted` – find deleted keys instead of keys with values. Defaults to false.

API design decisions:

- Timestamps are GMT and includes microseconds – and has the form `yyyy-mm-ddThh:mm:ss.ssssss`.
- All data are octets – mine-type Application/binary – this allows both binary data and text.
- Keys are percent-encoded binary strings with a maximum length of 255 octets. 
- Values has a maximum length of 16M (HTTP-servers usually has as maximum size for PUT-requests).

<br />

The special `_db` database contains a list of databases and permissions. Only writable by the admin user. The key is the name of the database, and the value must be JSON with non-ASCII characters escaped. The value contains the following properties:

```
{
  "permissions": {
    "create": ["some role"],
    "read": ["some role", "another role"],
    "update": ["some role"],
    "delete": ["some role"],
    "find": ["some role"],
    "forceVersion": ["some role"],
  },
  "notes": "optional description, can be omitted",
  "TTL": 3600,
  "deletedTTL": 5184000
}
```

- The `permissions` contains whitelists of roles that may do a given action
    - there is a special role `"*"` that includes every user
- The `deletedTTL` property is optional. If it is set, then history about deleted keys will only be kept for `deletedTTL` seconds. This saves space, but limits ability to synchronise and do incremental backups.
- The `TTL` property is optional. If it is set, then documents older than this number of seconds will regularly be deleted. This is useful if the database is used for communication between clients. Overrides `deletedTTL`.
- The `notes` property is optional


<br />


The special `_token` database contains a list of tokens, their expiry date and roles. The key is the actual token, and the value must be JSON with non-ASCII characters escaped. The value contains the following properties:

```
{
  "roles": ["some role", "another role"],
  "expires": "2100-01-01T00:00:00.000000",
  "notes": "optional description, can be omitted"
}
```

## Usage guide

Some examples of usage, and corresponding permissions.

- Private storage: only readable/writable/findable by owner.
- Inbox: `"*"` can create, and otherwise only readable/writable/findable by owner
- Public storage: readable/findable by `"*"` and only writable by owner
- Channel for webrtc signalling, or public pub-sub: readable/writable/findable by `"*"`, but with a short TTL, such as 60 sec. Use `find` with `since` and `wait` to subscribe to new values.
- Demo storage: long TTL such as 3600

Always have a TTL on databases that are both readable and writable by non-trusted users to avoid misuse.

HTTP/1-clients often has a very limited number of parallel connection, so host behind at HTTP/2 reverse proxy for best performance.

## Authentication

Web/apps should be able to login with OAuth2-pkce, to get access token and create needed databases. This is not implemented yet, but on the roadmap.

## Roadmap

- Servers
    - [ ] node.js implementation (under development)
    - [ ] PHP implementation
    - [ ] node.js caching
    - [ ] node.js optimised pub-sub
    - (java server)
    - (clustering)
- Testing, usable with any implementation
    - [ ] blackbox test (under development)
    - [ ] benchmark
- Admin
    - [ ] admin web/app
    - [ ] backup/mirroring scripts
- Auth
    - [ ] WordPress auth, incl. secret for client-side encryption
    - [ ] node.js passport auth, incl. secret for client-side encryption
- Client
    - [ ] js: leveldown adaptor
    - (js: synchronise to local leveldb)
    - (python: api)
    - (client-side encrypted data)
- Operations
    - [ ] WordPress plugin
    - [ ] Docker image
- Demo
    - [ ] updated web-site
    - [ ] swagger-api
    - [ ] hosted demo

## Blackbox testing

Test cases:

- get
    - [ ] read permission-denied
    - [ ] vanilla
    - [ ] 404 value
    - [ ] 404 database
- put
    - [ ] 404 database
    - [ ] write permission-denied
    - [ ] forceVersion permission-denied
    - [ ] vanilla
    - [ ] prevVersion
    - [ ] prevVersion conflict
    - [ ] prevVersion=null
    - [ ] prevVersion=null conflict
    - [ ] forceVersion
    - [ ] forceVersion + prevVersion
    - [ ] forceVersion + prevVersion conflict
    - [ ] forceVersion + prevVersion=null
    - [ ] forceVersion + prevVersion=null conflict
- del
    - [ ] permission-denied
    - [ ] vanilla
    - [ ] forceVersion
    - [ ] forceVersion + prevVersion
    - [ ] forceVersion + wrong prevVersion
- find
    - [ ] permission-denied
    - [ ] limit
    - [ ] lt
    - [ ] lte
    - [ ] gt
    - [ ] gte
    - [ ] since
    - [ ] reverse
    - [ ] wait
    - [ ] since+lt error
    - [ ] empty result
- misc
    - [ ] with empty key
    - [ ] with unicode key
    - [ ] with non-urisafe-key
    - [ ] with empty value
    - [ ] with unicode value
    - [ ] key \0 - \u00ff
    - [ ] value \0 - \u00ff
    - [ ] key maxlength handling/error
    - [ ] TTL
    - [ ] deleteTTL
    - [ ] invalid token

# &nbsp;

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

# Old/deprecated notes
## Simplebien - simple binary encoding/decoding

Simplebien is a binary encoding, that is designed to be simple to implement. 
JSON does not support binary data. CBOR and MsgPack have complexity and features not needed here. 
Here is the specification, followed by a pseudocode implementation:

Data has a 3 bit type and an unsigned integer N. These are encoded as a [Variable-Length Quantity](https://en.wikipedia.org/wiki/Variable-length_quantity), as `N * 8 + type`.

There are the following types:

- `type=0` positive integer = the number N
- `type=1` negative integer = bitwise negated N 
- `type=2` fixed-size value, where  N indicates the subtype:
    - `N=0` IEEE754 double float encoded in the next 8 bytes
    - `N=1` true value
    - `N=2` false value
    - `N=3` undefined/null value
- `type=3` dictionary with N key+value-object pairs encoded in the following bytes
- `type=4` array with N entries encoded in the following bytes
- `type=5` UTF-8 string, in the next N bytes
- `type=6` Binary data, in the next N bytes 

N is assumed to be at most 32 bit.

### Simplebien pseudocode implementation.

Entire pseudocode for implementation of encoder + decoder:

```
encodeTag(type, n) {
    encodeVariableLengthQuantity(n * 8 + type);
}

encode(o) {
    if(o.isInt()) {
        if(o >= 0) {
            encodeTag(0, o);
        } else {
            encodeTag(1, ~o);
        }
    }
    if(o.isFloat()) { encodeTag(2,0); encodeIEEE754DoublePrecisionFloat() }
    if(o == true) encodeTag(2,1);
    if(o == false) encodeTag(2,2);
    if(o == undefined) encodeTag(2,3);
    if(o.isDictionary()) {
        encodeTag(3, o.size());
        for((k,v) in o) {
            encode(k);
            encode(v);
        }
    }
    if(o.isArray()) { encodeTag(4, o.size()); for(v in o) encode(v); }
    if(o.isString()) { encodeTag(5, o.utf8ByteLength()); encodeUTF8(o); }
    if(o.isBinary()) { encodeTag(6, o.size()); encodeBytes(o); }
}

decode() {
    i = decodeVariableLengthQuantity()
    type = i % 8;
    n = (int) i / 8;
    switch(type) {
        case 0: return n;
        case 1: return ~n;
        case 2: switch(n) {
                case 0: return decodeFloat();
                case 1: return true;
                case 2: return false;
                case 3: return undefined;
            }
        case 3:
            result = new Dictionary();
            while(n--) {
                k = decode()
                v = decode()
                result.set(k, v);
            }
            return result
        case 4:
            result = new Array();
            while(n--) result.push(decode())
            return result
        case 5:
            return decodeUTF8(length: n)
        case 6:
            return decodeBytes(length: n)
        }
    }
}
```

## PHP-implementation notes
### Development
To develop:

1. `docker-compose up` runs wordpress + adminer + mariadb
2. `cd plugin; npm install ; npm run dev` auto-rebuilds js-app
3. `npm install; npm run dev` loads browsersync

Notes:

- http://localhost:3000/wp-content/plugins/mycrud/demo.html
- `plugin/demo.html` and `plugin/src/index.js` is demo/test-app
- `plugin/mycrud.php` is actual backend code
- php `error_log(...)`  logs to log/apache2/error.log

### Database

schema: see plugin/mycrud.php
```
CREATE TABLE mycrud_token (
    id INT NOT NULL AUTO_INCREMENT,
    token VARCHAR(15) NOT NULL,
    url VARCHAR(255) NOT NULL,
    owner INT NOT NULL,
    permissions TEXT NOT NULL,
    expires TIMESTAMP NOT NULL
);
```

### Order of implementation

- √PUT /mycrud/$DB (without handling data)
- √PUT /mycrud/$DB/$KEY
- √GET /mycrud/$DB/$KEY
- √DELETE /mycrud/$DB/$KEY
- √leveldb adaptor
- switch to wpapi instead of url (access-control done by wp-api and compatible plugins)
- convert leveldb to use wpapi
- GET /mycrud/$DB?lt/gt/gte/...
- leveldb adaptor find
- leveldb test-runner - https://github.com/Level/abstract-leveldown#test-suite
- refactor plugin code according to best practices
- proper handling of binary data
- GET /mycrud/
- GET /mycrud/$DB
- DELETE /mycrud/$DB
- ttl
- atomicity
- access-control: capabilities + anonymous
- Signalling client.
- capability access
- anonymous access
- batched requests
- unhosted
