import React from "react";
import ReactDOM from "react-dom";
import Button from "@material-ui/core/Button";

function App() {
  return (
    <Button variant="contained" color="primary">
      Hello World
    </Button>
  );
}

function main() {
  ReactDOM.render(<App />, document.querySelector("#app"));
}
main();
/*
if (module.hot) {
  module.hot.accept("./scene.ts", () => main({ scene, camera, engine, world }));
}
*/
