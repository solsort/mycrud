module.exports = {
  entry: './src/index.tsx',
	devServer: {
		hot: true,
		contentBase: __dirname,
		inline: true,
		host: "0.0.0.0",
		port: 3000,
		//       stats: "minimal",
	},
	resolve: {
		extensions: ['.ts', '.js', '.tsx']
	},
	module: {
		rules: [{
			test: /\.tsx?$/,
			loader: 'ts-loader',
      exclude: /node_modules/,
		}]
	}
}
