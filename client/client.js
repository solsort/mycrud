let { encode, decode } = require("../nodejs/src/simplebien.js");

const http = (fetch) => async (req) => {
  let { url, method, headers, body } = req;
  let opt = { method, headers, body };
  for (let k in opt) if (opt[k] === undefined) delete opt[k];
  let result = await fetch(url, opt);

  headers = {};
  for (const [k, v] of result.headers) {
    headers[k] = v;
  }
  result = {
    status: result.status,
    headers,
    body: await result.text(),
  };
  return result;
};
const qs = (o) =>
  o
    ? "?" +
      Object.keys(o)
        .map((k) => `${encodeURIComponent(k)}=${encodeURIComponent(o[k])}`)
        .join("&")
    : "";

const str2ArrayBuffer = s => Uint8Array.from(s.split('').map(c=>c.charCodeAt(0)))
let httpApi = (http) => (url) => (db, token) => {
  const tokenHeader = token ? { Authorization: "Bearer: " + token } : {};
  db = encodeURIComponent(db);
  return {
    get: async (key) => {
      let result = await http({
        url: `${url}${db}/${encodeURIComponent(key)}`,
        headers: tokenHeader,
      });
      if (result.status !== 200) throw new Error(JSON.stringify(result));
      return { v: result.body, ts: result.headers.etag.slice(1, -1) };
    },
    put: async (key, body, opt) => {
      headers = { "content-type": "application/binary", ...tokenHeader };
      if (opt && opt.contentType) headers["content-type"] = "application/json";
      let result = await http({
        method: "PUT",
        url: `${url}${db}/${encodeURIComponent(key)}${qs(opt)}`,
        headers,
        body: str2ArrayBuffer(body),
      });
      if (result.status !== 200) throw new Error(JSON.stringify(result));
      return result.body;
    },
    del: async (key, opt) => {
      let result = await http({
        method: "DELETE",
        url: `${url}${db}/${encodeURIComponent(key)}${qs(opt)}`,
        headers: tokenHeader,
      });
      if (result.status !== 200) throw new Error(JSON.stringify(result));
      return result.body;
    },
    find: async (opt) => {
      let result = await http({
        url: `${url}${db}${qs(opt)}`,
        headers: tokenHeader,
      });
      if (result.status !== 200) throw new Error(JSON.stringify(result));
      return JSON.parse(result.body);
    },
  };
};
let reqs = [];
let wsApi = (ws) => {
  let call = (...args) =>
    new Promise((resolve, reject) => {
      let seq = 0;
      while (reqs[seq] !== undefined) ++seq;
      reqs[seq] = { resolve, reject, args };
      ws.send(encode([seq].concat(args)));
    });
  setInterval(() => call('ping'), 9000) 

  ws.onmessage = async (d) => {
    let data;
    if (typeof Blob !== undefined && d.data.constructor === Blob) {
      data = await new Promise((resolve, reject) => {
        const fr = new FileReader();
        fr.onload = (e) => {
          try {
            resolve(decode(new Uint8Array(fr.result)));
          } catch (e) {
            reject(e);
          }
        };
        fr.readAsArrayBuffer(d.data);
      });
    } else {
      data = decode(d.data);
    }
    if (data.error) {
      let reject = reqs[data.seq].reject;
      reqs[data.seq] = undefined;
      return reject(new Error(data.error));
    }
    let [seq, result] = data;
    let resolve = reqs[seq].resolve;
    reqs[seq] = undefined;
    return resolve(data[1]);
  };
  let ready = new Promise((resolve) => (ws.onopen = resolve));
  ws.onclose = () => {};

  return async (db, token) => {
    await ready;
    let id = await call("open", db, token);
    return {
      get: (k) => call("get", id, encodeURIComponent(String(k))),
      put: (k, v, opt) =>
        call("put", id, encodeURIComponent(String(k)), v, opt),
      del: (k, opt) => call("del", id, encodeURIComponent(String(k)), opt),
      find: (opt) =>
        call(
          "find",
          id,
          ((o) => {
            for (const k in o) o[k] = String(o[k]);
            return o;
          })(opt)
        ),
      closeAll: () => ws.close(),
    };
  };
};

function mycrud(url, opt) {
  opt = opt || {};
  if (url.startsWith("http")) {
    let fetch = opt.fetch || self.fetch;
    let f;
    if (opt.httpMiddleware) {
      f = httpApi(opt.httpMiddleware(http(fetch)));
    } else {
      f = httpApi(http(fetch));
    }
    return { open: f(url) };
  }
  if (url.startsWith("ws")) {
    let WebSocket = opt.WebSocket || self.WebSocket;
    let ws = new WebSocket(url);
    return { open: wsApi(ws) };
  }
}

module.exports = { mycrud, connect: mycrud };
