require("./mycrud");
sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

async function main() {
  await sleep(1000);
  require("./test.js");
  await sleep(3500);
  process.exit();
}
main();
