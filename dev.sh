#. env.sh
(sleep 1; touch mycrud.js) &
  while inotifywait -e modify,close_write,move_self -q *.js */*.js
  do 
    kill `cat .pid$MYCRUD_PORT`
    sleep 0.5
    rm -rf mycrud.sqlite3 
    sleep 0.5
    kill -9 `cat .pid$MYCRUD_PORT`
    echo ===========================================================
    node mycrud.js $@ &
    echo $! > .pid$MYCRUD_PORT
    sleep 0.5;
    (node test.js ; echo test done) &
  done
