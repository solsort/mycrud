const { promisify } = require("util");
const sqlite3 = require("sqlite3");
const mysql = require("mysql2/promise");
const http = require("http");

class MyCRUD {
  // constants - id's of system databases
  static _db = 1;
  static _token = 2;

  // Util
  static sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
  static timestamp() {
    let [s, ns] = process.hrtime();
    let ms = (s * 1000 + ns / 1e6) | 0;
    if (!this.t0) this.t0 = Date.now() - ms;
    let d = new Date(this.t0 + ms).toISOString().slice(0, -1);
    d += (ns + 1e9).toString().slice(4, 7);
    return d;
  }
  static unixtime() {
    return (Date.now() / 1000) | 0;
  }
  static fromLatin1JSON(str) {
    try {
      return JSON.parse(Buffer.from(str, "latin1").toString("utf8"));
    } catch (e) {
      return null;
    }
  }
  static toLatin1JSON(obj) {
    let s = JSON.stringify(obj);
    return s.replace(
      /[^\0-\x7f]/g,
      (c) => "\\u" + (0x10000 + c.charCodeAt(0)).toString(16).slice(1)
    );
  }
  static percentEncode(str) {
    return str.replace(/[^-a-zA-Z0-9._~]/g, (s) => {
      return "%" + (256 + s.charCodeAt(0)).toString(16).slice(1).toUpperCase();
    });
  }
  static percentDecode(str) {
    return str.replace(/%(..)/g, (s) => {
      return String.fromCharCode(parseInt(s.slice(1), 16));
    });
  }
  checkPermission(permission) {
    for (const role of this.roles)
      for (const allowed of this.permissions[permission] || [])
        if (role === allowed) return true;
    this.done(403, permission + " not allowed");
  }

  // HTTP
  setupHTTP({ req, res, postData }) {
    this.req = req;
    this.res = res;
    this.postData = postData;
    let { method, url, headers } = req;
    this.url = url;
    this.method = method;
    this.headers = headers;
  }
  setResponseHeader(headerName, str) {
    this.res.setHeader(headerName, str);
  }
  connected() {
    return !this.req.aborted;
  }
  done(statusCode, content) {
    this.res.statusCode = statusCode;
    this.res.end(Buffer.from(content, "latin1"));
    let ok = statusCode >= 200 && statusCode < 300;
    let e = new Error(ok ? "ok" : content);
    e.httpSuccess = true;
    e.statusCode = statusCode;
    throw e;
  }
  corsHeaders() {
    if (this.headers["origin"]) {
      this.setResponseHeader(
        "Access-Control-Allow-Origin",
        this.headers["origin"]
      );
      this.setResponseHeader(
        "Access-Control-Allow-Methods",
        "GET,POST,PUT,DELETE"
      );
      this.setResponseHeader("Access-Control-Allow-Credentials", "true");
      this.setResponseHeader(
        "Access-Control-Allow-Headers",
        "Authorization, Content-Type"
      );
    }
  }

  // SQL
  static async sql(sql, args) {
    //console.log(sql +' ' + JSON.stringify(args));
    sql = sql.replace("MCTABLE", MyCRUD.sqlImpl.table);

    try {
      let result = await MyCRUD.sqlImpl.run(sql, args);
      //console.log(JSON.stringify(result));
      return result;
    } catch (e) {
      //console.log(JSON.stringify(e));
      return null;
    }
  }
  async get(db, k) {
    let result = await MyCRUD.sql(
      "SELECT id, v, ts FROM MCTABLE WHERE db=? AND k=?",
      [db, k]
    );
    result = (result && result[0]) || {};
    return result;
  }
  async insert(db, k, v, ts) {
    let result = await MyCRUD.sql(
      "INSERT INTO MCTABLE (db, deleted, k, v, ts) VALUES (?, ?, ?, ?, ?)",
      [db, v === null ? 1 : 0, k, v, ts]
    );
    if (!result) this.done(409, "conflict");
  }
  async replace(db, k, v, ts) {
    let result = await MyCRUD.sql(
      `INSERT INTO MCTABLE (db, deleted, k, v, ts) VALUES (?, ?, ?, ?, ?)`,
      [db, v === null ? 1 : 0, k, v, ts]
    );
    if (!result) {
      result = await MyCRUD.sql(
        "UPDATE MCTABLE SET deleted=?, v=?, ts=? WHERE db=? AND k=?",
        [v === null ? 1 : 0, v, ts, db, k]
      );
    }
    if (!result) this.done(500, "internal REPLACE error");
  }
  async update(db, k, v, prevVersion, ts) {
    let result = await MyCRUD.sql(
      "UPDATE MCTABLE SET deleted=?, v=?, ts=? WHERE db=? AND k=? AND ts=?",
      [v === null ? 1 : 0, v, ts, db, k, prevVersion]
    );
    const o = await this.get(db, k);
    if (o.ts !== ts || o.v !== v) this.done(409, "conflict");
  }
  async del(db, k, prevVersion) {
    let result;
    if (prevVersion) {
      result = await MyCRUD.sql(
        "DELETE FROM MCTABLE WHERE db=? AND k=? AND ts=?",
        [db, k, prevVersion]
      );
    } else {
      result = await MyCRUD.sql("DELETE FROM MCTABLE WHERE db=? AND k=?", [
        db,
        k,
      ]);
    }
    if (!result) this.done(500, "internal DELETE error");
  }
  async delOlder(db, deleted, unixtime) {
    await MyCRUD.sql("DELETE FROM MCTABLE WHERE db=? AND deleted=? AND ts<=?", [
      db,
      deleted ? 1 : 0,
      new Date(unixtime * 1000).toISOString(),
    ]);
  }
  async find(db, kOrTs, lt, lte, gt, gte, reverse, limit, deleted) {
    let where = ["db=?", "deleted=?"];
    let args = [db, deleted];
    if (typeof lt === "string") {
      where.push(kOrTs + "<?");
      args.push(lt);
    }
    if (typeof lte === "string") {
      where.push(kOrTs + "<=?");
      args.push(lte);
    }
    if (typeof gt === "string") {
      where.push(kOrTs + ">?");
      args.push(gt);
    }
    if (typeof gte === "string") {
      where.push(kOrTs + ">=?");
      args.push(gte);
    }
    let query = `SELECT k, ts FROM MCTABLE WHERE ${where.join(
      " AND "
    )} ORDER BY ${kOrTs} ${reverse ? "DESC" : "ASC"} LIMIT ${limit}`;
    let result = await MyCRUD.sql(query, args);
    return result.map((o) => [o.k, o.ts]);
  }
  async dbInitialised() {
    let o = await this.get(MyCRUD._db, "_db");
    if (o.id !== 1) return false;
  }
  async initialiseDB() {
    let _db = MyCRUD._db;
    let schema = `
      CREATE TABLE MCTABLE (
        id INTEGER PRIMARY KEY AUTO_INCREMENT,
        db INTEGER NOT NULL,
        k VARCHAR(255) NOT NULL,
        ts CHAR(26) NOT NULL,
        deleted INTEGER(1) NOT NULL,
        v TEXT(16777215) NULL,
        UNIQUE (db, deleted, k),
        UNIQUE (db, deleted, ts)
      ) DEFAULT CHARACTER SET latin1;`;
    if (MyCRUD.sqlImpl.dialect === "sqlite3") {
      schema = schema.replace("AUTO_INCREMENT", "");
      schema = schema.replace("DEFAULT CHARACTER SET latin1", "");
    }

    await MyCRUD.sql(schema);
    let _dbDoc = MyCRUD.toLatin1JSON({
      permissions: {
        create: ["admin"],
        read: ["*"],
        update: ["admin"],
        delete: ["admin"],
        find: ["admin"],
        forceVersion: [],
      },
    });
    await MyCRUD.sql(
      "INSERT INTO MCTABLE (id, db, deleted, k, v, ts) VALUES (?, ?, 0, ?, ?, ?)",
      [_db, _db, "_db", _dbDoc, MyCRUD.timestamp()]
    );
    let _tokenDoc = MyCRUD.toLatin1JSON({
      permissions: {
        create: ["admin"],
        read: ["admin"],
        update: ["admin"],
        delete: ["admin"],
        find: ["admin"],
        forceVersion: [],
      },
    });
    await MyCRUD.sql(
      "INSERT INTO MCTABLE (id, db, deleted, k, v, ts) VALUES (?, ?, 0, ?, ?, ?)",
      [MyCRUD._token, _db, "_token", _tokenDoc, MyCRUD.timestamp()]
    );
    let admin = MyCRUD.toLatin1JSON({
      roles: ["admin"],
      expires: "2100-01-01T00:00:00.000000",
    });
    await this.insert(
      MyCRUD._token,
      MyCRUD.adminToken,
      admin,
      MyCRUD.timestamp()
    );
  }

  // Application logic
  constructor(request) {
    this.setupHTTP(request);
    this.corsHeaders();
    this.main();
  }
  async main() {
    try {
      this.parseRequest();
      await this.loadDbAndRolesAndOptionallyInitialiseDB();
      this.handleTTL();
      this.checkPermissionsAndDetermineAction();
      await this.doAction();
      throw new Error("doAction should throw after action");
    } catch (e) {
      if (e.httpSuccess) return;
      try {
        this.done(500, "internal error");
      } catch (e) {}
      throw e;
    }
  }
  parseRequest() {
    // sets properties:  dbName, key, qs, token

    let url = this.url;
    let qs = (this.qs = {});
    let pos = url.lastIndexOf("?");
    if (pos !== -1) {
      let query = url.slice(pos + 1);
      let keyvals = query.split("&");
      for (const keyval of keyvals) {
        let i = keyval.indexOf("=");
        let k = keyval.slice(0, i);
        let v = keyval.slice(i + 1);
        qs[k] = MyCRUD.percentDecode(v);
      }
      url = url.slice(0, pos);
    }

    pos = url.lastIndexOf("/");
    let key = url.slice(pos + 1);
    if (pos !== -1) {
      url = url.slice(0, pos);
    }
    let dbName = url.slice(url.lastIndexOf("/") + 1);

    if (qs["lt"] || qs["lte"] || qs["gt"] || qs["gte"] || qs["since"]) {
      dbName = key;
      key = null;
    }

    this.dbName = dbName && MyCRUD.percentDecode(dbName);
    this.key = key && MyCRUD.percentDecode(key);
    if (this.dbName.length > 255 || (this.key && this.key.length > 255)) {
      this.done("400", "key too long");
    }
    let auth = this.headers["authorization"];
    this.token =
      auth && auth.startsWith("Bearer ") && auth.slice("Bearer ".length);

    if (this.method === "OPTIONS") {
      this.done(204, "");
    }
  }
  async loadDbAndRolesAndOptionallyInitialiseDB() {
    // sets properties: db, permissions, TTL, deletedTTL, lastClear, roles
    this.db = await this.get(MyCRUD._db, this.dbName);
    if (!this.db.v) {
      if (false === (await this.dbInitialised())) {
        await this.initialiseDB();
        this.done(503, "initialising database");
      }
      this.done(404, "database not found");
    }

    let o = MyCRUD.fromLatin1JSON(this.db.v);
    this.permissions = o.permissions;
    this.TTL = typeof o.TTL === "number" ? o.TTL : -1;
    this.deletedTTL = typeof o.deletedTTL === "number" ? o.deletedTTL : -1;
    this.lastClear = o.lastClear;

    this.roles = ["*"];
    if (this.token) {
      let auth = MyCRUD.fromLatin1JSON(
        (await this.get(MyCRUD._token, this.token)).v
      );
      if (auth && auth.expires && auth.expires > MyCRUD.timestamp()) {
        this.roles = auth.roles;
        this.roles.push("*");
      }
    }
  }
  async handleTTL() {
    let o;
    if (this.TTL > 0 || this.deletedTTL > 0) {
      if (this.deletedTTL >= 0) {
        this.deletedTTL = Math.min(this.deletedTTL, this.TTL);
      } else {
        this.deletedTTL = this.TTL;
      }

      let now = MyCRUD.unixtime();
      if (now - (this.lastClear || 0) > this.deletedTTL) {
        if (this.TTL > 0)
          await this.delOlder(this.db.id, false, now - this.TTL);
        await this.delOlder(this.db.id, true, now - this.deletedTTL);
        let db = await this.get(MyCRUD._db, this.dbName);
        o = MyCRUD.fromLatin1JSON(this.db.v);
        o.lastClear = now;
        if (o) {
          await this.update(
            MyCRUD._db,
            this.dbName,
            MyCRUD.toLatin1JSON(o),
            db.ts,
            MyCRUD.timestamp()
          );
        }
      }
    }
  }
  checkPermissionsAndDetermineAction() {
    // sets properties: action(find, get, update, create, replace, deleteVersion or delete), value
    if (this.qs["forceVersion"]) {
      this.checkPermission("forceVersion");
      this.ts = this.qs["forceVersion"];
    } else {
      this.ts = MyCRUD.timestamp();
    }
    if (this.method === "GET") {
      if (this.key === null) {
        this.checkPermission("find");
        this.action = "find";
      } else {
        this.checkPermission("read");
        this.action = "get";
      }
    }
    if (this.method === "PUT") {
      this.value = this.postData;

      if (this.qs["prevVersion"]) {
        if (
          "2000-01-01T00:00:00" < this.qs["prevVersion"] &&
          this.qs["prevVersion"] < "3000-01-01T00:00:00"
        ) {
          this.checkPermission("update");
          this.action = "update";
        } else {
          this.checkPermission("create");
          this.action = "create";
        }
      } else {
        this.checkPermission("create");
        this.checkPermission("update");
        this.action = "replace";
      }
    }

    if (this.method === "DELETE") {
      this.checkPermission("delete");
      this.value = null;

      if (this.deletedTTL === 0) {
        this.action = "delete";
      } else {
        if (this.qs["prevVersion"]) {
          this.action = "update";
        } else {
          this.action = "replace";
        }
      }
    }
  }
  async doAction() {
    let action = this.action;
    if (action === "get") {
      let result = await this.get(this.db.id, this.key);
      if (result.ts) {
        this.setResponseHeader("Content-Type", "text/plain;charset=ISO-8858-1");
        this.setResponseHeader("ETag", '"' + result.ts + '"');
        this.setResponseHeader("Last-Modified", (new Date(result.ts).slice(0,19) + 'Z').toUTCString());
        this.done(200, result.v);
      } else {
        this.done(404, "");
      }
    }
    if (action === "create") {
      await this.insert(this.db.id, this.key, this.value, this.ts);
      this.done(200, this.ts);
    }
    if (action === "update") {
      await this.update(
        this.db.id,
        this.key,
        this.value,
        this.qs["prevVersion"],
        this.ts
      );
      this.done(200, this.ts);
    }
    if (action === "replace") {
      await this.replace(this.db.id, this.key, this.value, this.ts);
      this.done(200, this.ts);
    }
    if (action === "delete") {
      await this.del(this.db.id, this.key, this.qs["prevVersion"]);
      this.done(200, "");
    }
    if (action === "find") {
      let qs = this.qs;
      let reverse = !!qs["reverse"];
      let limit = Math.min((+qs["limit"] || 100) & 0xffffff, 10000);
      let wait = Math.min(+qs["wait"] || 0, 300);
      let deleted = !!qs["deleted"];
      let kOrTs, gt, gte, lt, lte;
      if (this.qs["since"]) {
        kOrTs = "ts";
        gt = this.qs["since"];
      } else {
        kOrTs = "k";
        lt = this.qs["lt"];
        lte = this.qs["lte"];
        gt = this.qs["gt"];
        gte = this.qs["gte"];
      }
      let result = await this.find(
        this.db.id,
        kOrTs,
        lt,
        lte,
        gt,
        gte,
        reverse,
        limit,
        deleted
      );
      if (wait > 0 && result.length === 0 && this.connected()) {
        await MyCRUD.sleep(1000);
        --wait;
        if (this.connected())
          result = await this.find(
            this.db.id,
            kOrTs,
            lt,
            lte,
            gt,
            gte,
            reverse,
            limit,
            deleted
          );
      }

      result = result.map(([a, b]) => [MyCRUD.percentEncode(a), b]);
      this.setResponseHeader("Content-Type", "application/json");
      this.done(200, MyCRUD.toLatin1JSON(result));
    }
  }
}

async function setupDatabaseConnection() {
  let backend = process.env.MYCRUD_DB;

  MyCRUD.adminToken = process.env.MYCRUD_TOKEN;
  if (!MyCRUD.adminToken) throw new Error("missing MYCRUD_TOKEN env");

  if (backend.startsWith("mysql://")) {
    let [connect, database, table] = backend
      .slice("mysql://".length)
      .split("/");
    let [userpassword, host] = connect.split("@");
    let [user, password] = userpassword.split(":");
    //let con = await mysql.createConnection({
    let con = await mysql.createPool({
      waitForConnections: true,
      connectionLimit: 10,
      queueLimit: 0,
      charset: "LATIN1_SWEDISH_CI",
      host,
      user,
      password,
      host,
      database,
    });
    MyCRUD.sqlImpl = {
      run: async (sql, args) => (await con.query(sql, args))[0],
      dialect: "mysql",
      table
    };
  } else if (backend.startsWith("sqlite3://")) {
    let path = backend.slice("sqlite3://".length);
    let sql = new sqlite3.Database(path);
    MyCRUD.sqlImpl = {
      run: promisify(sql.all).bind(sql),
      dialect: "sqlite3",
      table: "mycrud"
    };
  } else {
    throw new Error("missing MYCRUD_BACKEND env");
  }
}
setupDatabaseConnection();
function startHTTPServer() {
  const port = process.env.MYCRUD_PORT || 8008;
  const server = http.createServer((req, res) => {
    let postData = "";
    req.on("data", (buf) => {
      if (postData.length < 16 * 1024 * 1024) {
        postData += buf.toString("latin1");
      }
    });
    req.on("end", () => {
      if (postData.length >= 16 * 1024 * 1024) {
        res.statusCode = 413;
        return res.end("Error: tried to put value larger than 16M");
      }
      return new MyCRUD({ req, res, postData });
    });
  });
  server.listen(port);
}
startHTTPServer();
