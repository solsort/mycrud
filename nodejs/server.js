let { promisify } = require("util");

let storage;
let backend = process.env.MYCRUD_BACKEND || "";
let port = process.env.MYCRUD_PORT || 8006;

if (backend.startsWith("mysql://")) {
  let [user, password, host, database] = backend
    .replace("mysql://", "")
    .split(/[[:@/]/g);

  const mysql2 = require("mysql2/promise");
  const pool = mysql2.createPool({
    user,
    host,
    password,
    database,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0,
  });

  storage = require("./src/storage.sql.js").storage({
    run: async (...args) => await pool.execute(...args),
    get: async (...args) => (await pool.execute(...args))[0][0],
    all: async (...args) => (await pool.execute(...args))[0],
    dialect: "mysql",
  });
} else if (backend.startsWith("leveldb:")) {
  storage = require("./src/storage.level.js").storage(
    require("level")(backend.replace("leveldb:", ""))
  );
} else if (backend.startsWith("sqlite3:")) {
  const sqlite3 = require("sqlite3");
  const sql = new sqlite3.Database(backend.replace("sqlite3:", ""));

  storage = require("./src/storage.sql.js").storage({
    run: promisify(sql.run).bind(sql),
    get: promisify(sql.get).bind(sql),
    all: promisify(sql.all).bind(sql),
    dialect: "sqlite3",
  });
} else {
  throw new Error("Missing MYCRUD_BACKEND environment");
}

const mycrud = require("./src/server.js");
const httpServer = require("./src/httpServer.js");
const wsServer = require("./src/wsServer.js");

mycrud.initialise(storage);
server = httpServer.serve({ prefix: "/", port});
wsServer.serve(server);
