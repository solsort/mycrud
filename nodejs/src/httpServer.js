const http = require("http");
let { mycrudConnection } = require("./server.js");
let { fromJSON, toJSON } = require("./util.js");
const fs = require('fs');

let pathPrefix = "";
function serve({ prefix, port }) {
  port = port || 8000;
  pathPrefix = prefix || pathPrefix;
  const server = http.createServer((req, res) => {
    //console.log(req.method, req.headers);
    if(req.headers.origin) {
      res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
      res.setHeader('Access-Control-Allow-Methods', "GET,POST,PUT,DELETE");
      res.setHeader('Access-Control-Allow-Credentials', "true");
      res.setHeader('Access-Control-Allow-Headers', "Authorization, Content-Type");
    }
    if(req.method === 'OPTIONS') {
      res.statusCode = 204;
      return res.end()
    }

    let body = "";
    req.on("data", (buf) => {
      if (body.length < 16 * 1024 * 1024) {
        body += buf.toString("latin1");
      }
    });
    req.on("end", async (o) => {
      if (body.length >= 16 * 1024 * 1024) {
        res.statusCode = 413;
        return res.end("Error: tried to send value larger than 16M");
      }

      let { method, url, headers } = req;
      const result = await handleRequest({ method, url, headers, body });
      if (!result.status) {
        console.error("ERROR invalid status code", result);
        result.status = 500;
      }
      res.statusCode = result.status;
      if (result.headers)
        for (const k in result.headers) {
          res.setHeader(k, result.headers[k]);
        }
      if (typeof result.body !== "string") {
        console.error("ERROR missing result body", result);
        result.body = "";
      }
      res.end(Buffer.from(result.body, "latin1"));
    });
  });
  server.listen(port);
  return server;
}

module.exports = { serve };

// HTTP api as: JSON in –> JSON out
async function handleRequest(opt) {
  let { method, url, headers, body } = opt;
  if (url.startsWith(pathPrefix)) url = url.slice(pathPrefix.length);

  function parsePath() {
    let [dbKey, query] = url.split("?");
    let [dbName, key] = dbKey.split("/");
    query = query ? query.split("&") : [];
    let args = {};
    for (const kv of query) {
      const [k, v] = kv.split("=");
      args[k] = v;
    }
    return { dbName, key, args };
  }

  try {
    let { dbName, key, args } = parsePath();
    let token =
      headers["authorization"] &&
      headers["authorization"].replace(/^Bearer:? +/, "");
    let db = await mycrudConnection(dbName, token);

    // `GET /`
    if (!dbName) {
      return {
        status: 200,
        headers: {
          "Content-Type": "text/html;charset=UTF-8",
        },
        body: `<!DOCTYPE html><html><head>
  <title>MyCrud web database</title>
  <style>pre { font: 12px sans-serif }</style>
</head><body>
<a href="https://mycrud.com">MyCrud</a> web database server.
</body></html>`,
      };
    }

    // `GET /$DB/$KEY`
    if (method === "GET" && key !== undefined) {
      let result = await db.get(key);
      if (!result.ts) return { status: 404, body: "" };
      let { v, ts } = result;
      return {
        status: 200,
        headers: {
          ETag: `"${ts}"`,
          "Content-Type":  "text/plain;charset=ISO-8859-1",
        },
        body: v,
      };
    }

    // `DELETE /$DB/$KEY[?prevVersion=...][&forceVersion=...][?force=true]`
    if (method === "DELETE" && key !== undefined) {
      await db.del(key, args);
      return { status: 200, body: "" };
    }

    // `PUT /$DB/$KEY[?prevVersion=...][&forceVersion=...]`
    if (method === "PUT" && key !== undefined) {
      let result = await db.put(key, body, args);
      return {
        status: 200,
        headers: { "Content-Type": "text/plain;charset=ISO-8859-1" },
        body: result,
      };
    }

    // `GET /$DB[?gt=...][&gte=...][&lt=...][&lte=...][&after=...][&before=...][?reverse=true][?limit=...]
    if (method === "GET" && key === undefined) {
      return {
        status: 200,
        headers: { "Content-Type": "application/json" },
        body: toJSON(await db.find(args)),
      };
    }
  } catch (e) {
    let parsedError = fromJSON(e.message) || {
      status: 500,
      body: "unexpected error",
    };
    if (parsedError.status >= 500) console.error("ERROR", e);
    return parsedError;
  }
}
