// imports
const {
  httpError,
  sleep,
  fromJSON,
  toJSON,
  uri_to_latin1,
  latin1_to_uri,
} = require("./util.js");

let storage;
async function initialise(storageImpl) {
  storage = storageImpl;
  const initialAdminToken = process.env.MYCRUD_TOKEN;
  if (!initialAdminToken) {
    console.error("Missing MYCRUD_TOKEN environment default token");
    process.exit(1);
  }
  if (!(await storage.initialised())) {
    await storage.initialise();
    await storage.insert(
      0,
      "_db",
      toJSON({
        _id: 0,
        _owner: "admin",
        _seq: 1,
        contentType: "application/json",
        perDocumentOwner: true,
        permissions: {
          create: ["admin"],
          read: ["*"],
          update: ["admin"],
          del: ["admin"],
          find: ["admin"],
          forceDelete: ["admin"],
          forceVersion: ["admin"],
        },
      })
    );
    await storage.insert(
      0,
      "_token",
      toJSON({
        _id: 1,
        _owner: "admin",
        contentType: "application/json",
        permissions: {
          create: ["admin"],
          read: ["*"],
          update: ["admin"],
          del: ["admin"],
          find: ["admin"],
          forceDelete: ["admin"],
          forceVersion: ["admin"],
        },
      })
    );
    await storage.insert(
      1,
      initialAdminToken,
      toJSON({
        user: "admin",
        roles: ["admin"],
        expires: "9999-01-01T00:00:00",
      })
    );
  }
}

// API on top of storage
async function mycrudConnection(dbName, token) {
  dbName = dbName && uri_to_latin1(dbName);

  let schemaVTS = await storage.get(0, dbName);
  let schema = (dbName && fromJSON(schemaVTS.v)) || {};
  let db = schema._id;
  let auth = token && fromJSON((await storage.get(1, token)).v);
  if (!(auth && auth.expires && new Date(auth.expires) > new Date()))
    auth = auth || { user: "anonymous", roles: [] };
  auth.roles.push("*");

  let doc;
  async function loadOwnedDocForPermissionTest(key) {
    if (schema.perDocumentOwner) doc = fromJSON((await storage.get(db, key)).v);
  }
  function isDocumentOwner() {
    return (
      schema.perDocumentOwner && doc && doc._owner && doc._owner === auth.user
    );
  }
  function hasPermission(permission) {
    if (typeof schema._id !== "number") httpError(404, "database not found");
    if (auth.user === "admin") return true;
    for (const requiredRole of schema.permissions[permission] || [])
      for (const role of auth.roles) if (role === requiredRole) return true;
  }
  function verifyPermission(permission) {
    if (
      !(
        hasPermission(permission) ||
        (isDocumentOwner() && !permission.startsWith("force"))
      )
    )
      httpError(403, "permission denied: " + permission);
  }

  async function get(key) {
    key = uri_to_latin1(key);
    await loadOwnedDocForPermissionTest(key);

    verifyPermission("read");
    let result = await storage.get(db, key);
    result.contentType = schema.contentType;
    return result;
  }
  async function put(key, value, opt) {
    key = uri_to_latin1(key);
    await loadOwnedDocForPermissionTest(key);

    let { prevVersion, forceVersion } = opt || {};
    if (forceVersion) verifyPermission("forceVersion");

    if (schema.perDocumentOwner) {
      doc = doc || {};
      doc._owner = doc._owner || auth.user;
      if (typeof doc._id !== "number") {
        schema._seq = (schema._seq || 0) + 1;
        doc._id = schema._seq;
        await storage.update(0, dbName, toJSON(schema), schemaVTS.ts);
      }

      value = fromJSON(value) || {};
      for (const k in value) {
        if (k[0] !== "_" || hasPermission("update")) {
          if (value[k] === undefined) {
            delete doc[k];
          } else {
            doc[k] = value[k];
          }
        }
      }
      value = toJSON(doc);
    }

    if (
      prevVersion !== undefined &&
      String(prevVersion).toLowerCase() === "null"
    ) {
      verifyPermission("create");
      return await storage.insert(db, key, value, forceVersion);
    }
    if (prevVersion === undefined) {
      verifyPermission("create");
      verifyPermission("update");
      return await storage.upsert(db, key, value, forceVersion);
    }
    if (prevVersion) {
      verifyPermission("update");
      prevVersion = decodeURIComponent(prevVersion);
      return await storage.update(db, key, value, prevVersion, forceVersion);
    }
  }
  async function del(key, opt) {
    key = uri_to_latin1(key);

    const { force, prevVersion, forceVersion } = opt;
    if (force) {
      verifyPermission("forceDelete");
      return storage.del(db, uri_to_latin1(key), prevVersion);
    }

    verifyPermission("delete");
    if (prevVersion === undefined) {
      return await storage.upsert(db, key, null, forceVersion);
    }
    if (prevVersion) {
      prevVersion = decodeURIComponent(prevVersion);
      return await storage.update(db, key, null, prevVersion, forceVersion);
    }
  }
  async function findImpl(opt) {
    verifyPermission("find");
    let args = {};
    for (const key in opt) {
      args[key] = uri_to_latin1(opt[key]);
    }
    args.limit = Math.min(args.limit || 1000, 1000);
    args.reverse = !!args.reverse;
    let { gt, gte, lt, lte, after, before, reverse, limit } = args;
    if ((gt || gte || lt || lte) && (before || after))
      httpError(400, "either search on key or timespan");
    let result;
    if (before || after) {
      let query = { reverse, limit };
      if (after) query.gt = after;
      if (before) query.lt = before;
      result = await storage.find(db, "ts", query);
    } else {
      result = await storage.find(db, "k", args);
    }
    result = result.map(([k, ts]) => [latin1_to_uri(k), ts]);
    return result;
  }
  async function find(opt) {
    // TODO better single-node implementation would be listen on put events,
    // (though that would only work with one server instance).

    let wait = Math.min(10, +opt.wait || 0);
    if (wait) verifyPermission("wait");
    let result = await findImpl(opt);
    while (wait > 0 && !result.length) {
      wait -= 0.5;
      await sleep(500);
      result = await findImpl(opt);
    }
    return result;
  }
  return { get, put, del, find };
}

module.exports = { initialise, mycrudConnection };
