const { promisify } = require("util");
const { sleep, httpError, getTimeStamp } = require("./util.js");
const { encode, decode } = require("./simplebien");
let DB;

/*
Sections of database:

- v: db k -> v
- t: db k -> ts
- k: db ts -> k
 

*/

locks = {};
async function locked(db, k, fn) {
  let result;
  let key = `${db} ${k}`;
  while (locks[key]) await sleep(1);
  locks[key] = true;
  try {
    result = await fn();
  } catch (e) {
    locks[key] = false;
    throw e;
  }
  locks[key] = false;
  return result;
}

async function getTs(db, k) {
  try {
    return await DB.get(`t${db} ${k}`);
  } catch (e) {
    return undefined;
  }
}
async function getV(db, k) {
  try {
    return await DB.get(`v${db} ${k}`);
  } catch (e) {
    return undefined;
  }
}
async function get(db, k) {
  return { v: await getV(db, k), ts: await getTs(db, k) };
}
async function doUpsert(prev, db, k, v, forceVersion) {
  const ts = forceVersion || getTimeStamp();
  await DB.put(`t${db} ${k}`, ts);
  await DB.put(`v${db} ${k}`, v);
  await DB.put(`k${db} ${ts}`, k);
  if (prev) await DB.del(`v${db} ${prev}`);
  return ts;
}
async function update(db, k, v, prevVersion, forceVersion) {
  const prev = await getTs(db, k);
  if (prev !== prevVersion) httpError(409, "conflict");
  return await doUpsert(prev, db, k, v, forceVersion);
}
async function upsert(db, k, v, forceVersion) {
  return await doUpsert(await getTs(db, k), db, k, v, forceVersion);
}
async function insert(db, k, v, forceVersion) {
  const prev = await getTs(db, k);
  if (prev !== undefined) httpError(409, "conflict");
  return await doUpsert(prev, db, k, v, forceVersion);
}
async function del(db, k, prevVersion) {
  const prev = await getTs(db, k);
  await DB.del(`t${db} ${k}`);
  await DB.del(`v${db} ${k}`);
  await DB.del(`v${db} ${prev}`);
}
function find(db, kOrTs, o) {
  return new Promise((resolve, reject) => {
    let prefix = kOrTs === "k" ? `t${db} ` : `k${db} `;
    o.limit = +o.limit;
    o.reverse = Boolean(o.reverse);
    for (const op of ["gt", "gte", "lt", "lte"])
      if (o[op] !== undefined) o[op] = prefix + o[op];
    let result = [];
    if (!(o.gte || o.gt)) o.gt = prefix;
    if (!(o.lte || o.lt)) o.lt = prefix.slice(0, -1) + "a";
    DB.createReadStream(o)
      .on("data", (o) => result.push([o.key.slice(prefix.length), o.value]))
      .on("close", () => {
        if (kOrTs === "ts") {
          result = result.map(([a, b]) => [b, a]);
        }
        resolve(result);
      });
  });
}
async function initialised() {
  return (await getTs(0, "_db")) !== undefined;
}
async function initialise() {}
module.exports = {
  storage: (db) => {
    DB = db;
    return { get, update, upsert, insert, del, find, initialised, initialise };
  },
};
