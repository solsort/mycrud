const { promisify } = require("util");
const { httpError, getTimeStamp } = require("./util.js");

let sql;

async function getTs(db, k) {
  let result = await sql.get("SELECT ts FROM mycrud WHERE db=? AND k=?", [
    db,
    k,
  ]);
  return result ? result.ts : null;
}
async function get(db, k) {
  let result = await sql.get("SELECT v, ts FROM mycrud WHERE db=? AND k=?", [
    db,
    k,
  ]);
  return result || {};
}
async function update(db, k, v, prevVersion, forceVersion) {
  const ts = forceVersion || getTimeStamp();
  await sql.run(`UPDATE mycrud SET v=?, ts=? WHERE db=? AND k=? AND ts=?`, [
    v,
    ts,
    db,
    k,
    prevVersion,
  ]);
  let newTs = await getTs(db, k);
  if (newTs !== ts) httpError(409, "conflict");

  return newTs;
}
async function upsert(db, k, v, forceVersion) {
  const ts = forceVersion || getTimeStamp();
  await sql.run("REPLACE INTO mycrud (db,k,v,ts) VALUES (?,?,?,?)", [
    db,
    k,
    v,
    ts,
  ]);
  return forceVersion || (await getTs(db, k));
}
async function insert(db, k, v, forceVersion) {
  const ts = forceVersion || getTimeStamp();
  await sql.run("INSERT INTO mycrud (db, k, v, ts) VALUES(?, ?, ?, ?)", [
    db,
    k,
    v,
    ts,
  ]);
  return forceVersion || (await getTs(db, k));
}
async function del(db, k, prevVersion) {
  if (prevVersion) {
    await sql.run("DELETE FROM mycrud WHERE db=? AND k=? AND ts=?", [
      db,
      k,
      prevVersion,
    ]);
    if (await getTs(db, k)) httpError(409, "conflict");
  } else {
    await sql.run("DELETE FROM mycrud WHERE db=? AND k=?", [db, k]);
  }
}
async function find(db, kOrTs, { gt, gte, lte, lt, limit, reverse }) {
  let where = ["db=?"];
  let args = [db];
  if (typeof gt === "string") {
    where.push(`${kOrTs}>?`);
    args.push(gt);
  }
  if (typeof gte === "string") {
    where.push(`${kOrTs}>=?`);
    args.push(gte);
  }
  if (typeof lt === "string") {
    where.push(`${kOrTs}<?`);
    args.push(lt);
  }
  if (typeof lte === "string") {
    where.push(`${kOrTs}<=?`);
    args.push(lte);
  }
  let query = `SELECT k, ts FROM mycrud WHERE ${where.join(
    " AND "
  )} ORDER BY ${kOrTs} ${reverse ? "DESC" : "ASC"} LIMIT ${Math.min(
    1000,
    +limit || 1000
  )}`;
  let result = await sql.all(query, args);
  return result.map((o) => [o.k, o.ts]);
}
async function initialised() {
  try {
    await sql.get("SELECT 1 FROM mycrud LIMIT 1");
    return true;
  } catch (e) {
    return false;
  }
}
async function initialise() {
  let x = await sql.run(`
      CREATE TABLE IF NOT EXISTS mycrud (
        db INT NOT NULL,
        k VARCHAR(255) NOT NULL,
        v MEDIUMTEXT,
        ts CHAR(26) NOT NULL,
        PRIMARY KEY (db, k),
        UNIQUE (db, ts)
      )${/*sql.dialect === "sqlite3" ? "" : 'CHAR SET = "latin1"'*/''}
    `);
}
module.exports = {
  storage: (sqlImpl) => {
    sql = sqlImpl;
    return { get, update, upsert, insert, del, find, initialised, initialise };
  },
};
