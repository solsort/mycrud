let t0, prevTime;
function getTimeStamp() {
  let result;
  do {
    let hrtime = process.hrtime();
    let ms = (hrtime[0] * 1000 + hrtime[1] / 1000000) | 0;
    let us = ((hrtime[1] / 1000) | 0) % 1000;
    if (!t0) {
      t0 = Date.now() - ms;
    }
    result =
      new Date(ms + t0).toISOString().slice(0, 23) +
      (1000 + us).toString().slice(1);
  } while (result === prevTime);
  prevTime = result;
  return result;
}
function fromJSON(json) {
  try {
    return JSON.parse(json);
  } catch (e) {
    return null;
  }
}
function toJSON(o) {
  return JSON.stringify(o).replace(
    /[\u0080-\uffff]/g,
    (s) => `\\u${(s.charCodeAt(0) + 0x10000).toString(16).slice(1)}`
  );
}
function httpError(code, err) {
  throw new Error(JSON.stringify({ status: code, body: err }));
}
function uri_to_latin1(s) {
  // latin1 uri decode
  return s.replace(/%../g, (s) =>
    String.fromCharCode(parseInt(s.slice(1), 16))
  );
}
function latin1_to_uri(s) {
  // latin1 uri decode
  return s.replace(
    /[^-ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789._$]/g,
    (c) => {
      c = c.charCodeAt(0);
      if (c > 255) httpError(400, "expected binary/latin1, got unicode");
      return "%" + (0x100 + c).toString(16).slice(1).toUpperCase();
    }
  );
}
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

module.exports = {
  httpError,
  sleep,
  getTimeStamp,
  fromJSON,
  toJSON,
  latin1_to_uri,
  uri_to_latin1,
};
