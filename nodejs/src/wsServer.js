const WebSocket = require("ws");
const { mycrudConnection } = require("./server");
const { encode, decode } = require("./simplebien");
async function serve(server) {
  const wss = new WebSocket.Server({ server });
  wss.on("connection", (ws) => {
    let dbs = [];
    async function handleMessage(args) {
      if (args[0] === 'ping') {
        return 'pong'
      } else if (args[0] === "open") {
        dbs.push(args.slice(1));
        return dbs.length - 1;
      } else {
        let [db, token] = dbs[args[1]];
        return (await mycrudConnection(db, token))[args[0]].apply(
          null,
          args.slice(2)
        );
      }
    }
    ws.on("message", async (msg) => {
      msg = decode(msg);
      let [seq] = msg;
      try {
        let result = await handleMessage(msg.slice(1));
        ws.send(encode([seq, result]));
      } catch (e) {
        ws.send(encode({ error: e.message, seq }));
      }
    });
  });
}

module.exports = { serve };
