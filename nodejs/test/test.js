let fetch = require("node-fetch");
let WebSocket = require("isomorphic-ws");

let {
  assert,
  logHttp,
  recordSnapshot,
  saveSnapshot,
} = require("./testUtil.js");
let { http, api, mycrud } = require("../../client/client.js");

const token = process.env.MYCRUD_TOKEN || "m97tstsw9dpik71433rv7b";
async function main() {
  console.time("test");
  let url = "http://localhost:8007/";

  let action;
  action = "logTest";
  //action = 'recordHttpSnapshot'
  //action = 'websocket'
  let con;
  if (action === "recordHttpSnapshot") {
    con = await mycrud(url, { fetch, httpMiddleware: recordSnapshot });
    //open = api(recordSnapshot(http(fetch)))("http://localhost:8000/");
  } else if (action === "logTest") {
    con = await mycrud(url, { fetch, httpMiddleware: logHttp });
    //open = api(logHttp(http(fetch)))("http://localhost:8000/");
  } else if (action === "websocket") {
    con = await mycrud(url.replace(/http/, "ws"), { WebSocket });
  } else {
    con = await mycrud(url, { fetch });
  }
  let { open } = con;

  let _db = await open("_db", token);
  let _token = await open("_token", token);
  let testDB = await open("testDB", "testToken");

  await _db.put(
    "testDB",
    JSON.stringify({
      permissions: {
        create: ["test"],
        find: ["*"],
        read: ["*"],
        update: ["test"],
        delete: ["test"],
        forceDelete: ["test"],
      },
    })
  );

  await _token.put(
    "testToken",
    JSON.stringify({
      user: "test",
      roles: ["test"],
      canCreateDB: true,
      expires: "9999-01-01T00:00:00",
    })
  );

  await testDB.put("hel lo", "world");
  result = await testDB.get("hel lo");
  assert((await testDB.get("hel lo")).v === "world");

  await testDB.put("hel lo", "w0rld", { prevVersion: result.ts });

  await assert.throws(
    () => testDB.put("hel lo", "welt", { prevVersion: result.ts }),
    (e) => assert(JSON.parse(e.message).status === 409)
  );

  for (let i = 0; i <= 10; ++i) {
    await testDB.put(i, "char: '" + String.fromCharCode(i) + "'");
  }
  await testDB.find({ gte: "2", limit: 5 });
  await testDB.find({ lte: "2", limit: 5 });
  await testDB.find({ limit: 10, reverse: true });

  console.log(await testDB.get(0));

  if (action === "recordSnapshot") {
    saveSnapshot();
  }
  let all = await testDB.find({});
  for (const [k, ts] of all) {
    await testDB.del(k, { force: true });
  }
  let testStr = '';
  for(let i = 0; i < 256; ++i){
    testStr += String.fromCharCode(i);
  }
  await testDB.put("testStr", testStr);
  result = await testDB.get('testStr');
  console.log(result.v.split('').map(c => c.charCodeAt(0)).join(' '))
  testDB.closeAll && testDB.closeAll();
  console.timeEnd("test");
}
main();
/* Test checklist:

- api
  - with empty key
  - with unicode key
  - with non-urisafe-key
  - with non-string key
  - with empty value
  - with unicode value
  - with non-string value
- get
  - read permission-denied
  - vanilla
  - 404 value
  - 404 database
- put
  - 404 database
  - write permission-denied
  - forceVersion permission-denied
  - vanilla
  - prevVersion
  - prevVersion conflict
  - prevVersion=null
  - prevVersion=null conflict
  - forceVersion
  - forceVersion + prevVersion
  - forceVersion + prevVersion conflict
  - forceVersion + prevVersion=null
  - forceVersion + prevVersion=null conflict
- del
  - permission-denied
  - vanilla
  - forceVersion
  - forceVersion + prevVersion
  - forceVersion + wrong prevVersion
- find
  - permission-denied
  - limit
  - lt
  - lte
  - gt
  - gte
  - before
  - after
  - reverse
  - TODO combi
- setSchema
  - add property
  - create permission-denied
  - change permission-denied
- permissions
  - invalid token

*/
