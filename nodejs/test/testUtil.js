function assert(t) {
  if (!t) {
    throw new Error("assert false");
  }
}
assert.throws = async (fn, handler) => {
  try {
    await fn();
  } catch (e) {
    return handler && handler(e);
  }
  throw new Error("assert didn't throw");
};
assert.eq = (a, b) => {
  if (a !== b) {
    throw new Error(`assert not equal:
        ${a}
        ${b}`);
  }
};
assert.deepEq = (a, b) => {
  a = JSON.stringify(a);
  b = JSON.stringify(b);
  if (a !== b) {
    throw new Error(`assert not deep equal:
        ${a}
        ${b}`);
  }
};

const logHttp = (http) => async (req) => {
  console.log("HTTP", req);
  let result = await http(req);
  console.log(" –>", result);
  return result;
};
const testName = "test";
const snapshot = {};
let snapshotIdx = 0;
function cleanSnapshot(result) {
  let testedHeaders = "content-type authorization etag";
  let s = JSON.stringify(result);
  s = s.replace(
    /20\d\d-\d\d-\d\dT\d\d:\d\d:\d\d.\d\d\d\d\d\d/g,
    "2020-01-02T03:04:05.067890"
  );
  result = JSON.parse(s);
  for (const k in result.headers)
    if (!testedHeaders.includes(k.toLowerCase())) delete result.headers[k];
  return result;
}
const recordSnapshot = (http) => async (req) => {
  let result = await http(req);
  snapshot[`${testName}-${++snapshotIdx}`] = [
    cleanSnapshot(req),
    cleanSnapshot(result),
  ];
  return result;
};
let saveSnapshot = () =>
  require("fs").writeFileSync(
    "test/snapshot.json",
    JSON.stringify(snapshot, null, 2)
  );
module.exports = {
  assert,
  logHttp,
  recordSnapshot,
  saveSnapshot,
};
