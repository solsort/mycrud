(() => {
  const isBrowser = typeof self !== "undefined";
  const { encode, decode } =
    (isBrowser && self.mycrudSimplebien) || require("./simplebien.js");

  function mycrudConnection({ ws }) {
    let seq = 0;
    let promises = new Map();
    ws.onmessage = ([seq, data]) => {
      if (typeof data === "object" && data.error) {
        promises.get(seq).reject(new Error(data.error));
      } else {
        promises.get(seq).resolve(data);
      }
      promises.delete(seq);
    };
    const call = (...args) =>
      new Promise(async (resolve, reject) => {
        await ws.ready;
        const i = seq++;
        try {
          promises.set(i, { resolve, reject });
          await ws.send([i, args]);
        } catch (e) {
          promises.delete(i);
          reject(e);
        }
      });
    return {
      close: () => ws.close(),
      open: ({ db, token }) => ({
        get: async (key) => decode((await call("get", token, db, key))[1]),
        getWithVersion: async (key) => {
          let [version, value] = await call("get", token, db, key);
          return { value: decode(value), version };
        },
        put: (key, value, prevVersion) =>
          call("put", token, db, key, encode(value), prevVersion),
        since: (version) => call("since", token, db, version),
        forceDelete: (key, prevVersion) =>
          call("del", token, db, key, prevVersion),
        delete: (key, prevVersion) =>
          call("put", token, db, key, encode(undefined), prevVersion),
        allKeys: async function () {
          let time = 0;
          let result = [];
          for (;;) {
            let pairs = await this.since(time);
            if (!pairs.length) {
              return result;
            }
            for (const pair of pairs) {
              result.push(pair);
            }
            time = pairs[pairs.length - 1][0];
          }
        },
      }),
    };
  }

  function connect(url) {
    let con = { onmessage: () => {} };
    if (isBrowser) {
      const ws = new window.WebSocket(url);
      con.close = () => ws.close();
      con.ready = new Promise((resolve, reject) => {
        ws.onopen = () => resolve();
        ws.onerror = (e) => reject(e);
      });
      con.send = (data) => ws.send(encode(data));
      ws.onmessage = async (event) => {
        let data = await new Response(event.data).arrayBuffer();
        con.onmessage(decode(new Uint8Array(data)));
      };
    } else {
      const WebSocket = require("ws");
      const ws = new WebSocket(url);
      con.close = () => ws.close();
      con.ready = new Promise((resolve, reject) => {
        ws.on("open", () => resolve());
        ws.on("error", () => reject("connection error"));
      });
      con.send = (data) => ws.send(encode(data));
      ws.on("message", (data) => {
        data = decode(data);
        con.onmessage(data);
      });
    }
    return mycrudConnection({ ws: con });
  }

  let mycrud = { connect, encode, decode };
  if (typeof module !== "undefined") module.exports = mycrud;
  if (typeof self !== "undefined") self.mycrud = mycrud;
})();
