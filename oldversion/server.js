const level = require("level");
const WebSocket = require("ws");
const { encode, decode } = require("./simplebien");

const port = process.env.MYCRUD_PORT;
if (!port) throw "MYCRUD_PORT not defined";

const supertoken = process.env.MYCRUD_TOKEN;
if (!supertoken) throw new Error("MYCRUD_TOKEN environment missing");

const enc = { keyEncoding: "binary", valueEncoding: "binary" };
const values = level("_mycrud_values.leveldb", enc);
const versions = level("_mycrud_versions.leveldb", enc);

const uniqueVersion = (() => {
  let prevUniqueVersion = 0;
  return async function uniqueVersion() {
    let version = Math.max((Date.now() - 1) * 1000, prevUniqueVersion + 1);
    prevUniqueVersion = version;
    while (version >= Date.now() * 1000) {
      await new Promise((resolve) => process.nextTick(resolve));
    }
    return version;
  };
})();

let locks = new Map();
async function getDbKeyWriteLock(key) {
  const keyStr = String.fromCharCode.apply(String, key);
  while (locks.get(keyStr)) {
    await new Promise((resolve) => setTimeout(resolve, 0));
  }
  locks.set(keyStr, true);
}
async function releaseDbKeyWriteLock(key) {
  // Throttle writes/deletes to same key to 100ms, as node-level seems to have problem keeping the order of operations otherwise :(
  // TODO better caching where put/delete throttle is included.
  setTimeout(() => locks.delete(String.fromCharCode.apply(String, key)), 100);
}
const storage = {
  put: async (db, key, value, expectedPrevVersion, version) => {
    const dbKey = encode([db, key]);
    await getDbKeyWriteLock(dbKey);
    try {
      let prevVersion = 0;
      try {
        prevVersion = decode(await values.get(dbKey))[0];
      } catch (e) {}
      if (
        typeof expectedPrevVersion === "number" &&
        expectedPrevVersion !== prevVersion
      ) {
        throw new Error("conflict");
      }
      const versionValue = encode([version, value]);
      const promise = values.put(dbKey, versionValue);

      // Using big-endian ensures that lexicographical byte comparison orders positive floating numbers
      const dbVersion = Buffer.alloc(12);
      dbVersion.writeUInt32BE(db, 0);
      dbVersion.writeDoubleBE(version, 4);
      await versions.put(dbVersion, encode(key));

      if (prevVersion) {
        const dbPrevVersion = Buffer.alloc(12);
        dbPrevVersion.writeUInt32BE(db, 0);
        dbPrevVersion.writeDoubleBE(prevVersion, 4);
        await versions.del(dbPrevVersion);
      }

      await promise;

      releaseDbKeyWriteLock(dbKey);
    } catch (e) {
      releaseDbKeyWriteLock(dbKey);
      throw e;
    }
  },
  get: async (db, key) => {
    const k = encode([db, key]);
    let result;
    try {
      result = await values.get(k);
      result = decode(result);
    } catch (e) {
      result = [0, encode(undefined)];
    }
    return result;
  },
  since: (db, version) =>
    new Promise((resolve, reject) => {
      var k = Buffer.alloc(12);
      k.writeUInt32BE(db, 0);
      k.writeDoubleBE(version, 4);

      let result = [];
      let iter = versions.createReadStream({ gt: k, limit: 100 });
      iter.on("data", (d) => {
        let dataDb = d.key.readUInt32BE(0);
        let version = d.key.readDoubleBE(4);
        let key = decode(d.value);
        if (dataDb === db) {
          result.push([version, key]);
        }
      });
      iter.on("error", (err) => reject(err));
      iter.on("end", () => resolve(result));
    }),
  del: async (db, key, expectedPrevVersion) => {
    const dbKey = encode([db, key]);
    await getDbKeyWriteLock(dbKey);
    try {
      if(typeof expectedPrevVersion === 'number') {
        const dbPrevVersion = Buffer.alloc(12);
        dbPrevVersion.writeUInt32BE(db, 0);
        dbPrevVersion.writeDoubleBE(expectedPrevVersion, 4);
        versions.del(dbPrevVersion);
      }
      let prevVersion = 0;
      try {
        prevVersion = decode(await values.get(dbKey))[0];
      } catch (e) {}
      if (
        typeof expectedPrevVersion === "number" &&
        expectedPrevVersion !== prevVersion
      ) {
        throw new Error("conflict");
      }
      const promise = values.del(dbKey);
      if (prevVersion) {
        const dbPrevVersion = Buffer.alloc(12);
        dbPrevVersion.writeUInt32BE(db, 0);
        dbPrevVersion.writeDoubleBE(prevVersion, 4);
        await versions.del(dbPrevVersion);
      }
      await promise;
      releaseDbKeyWriteLock(dbKey);
      return true;
    } catch (e) {
      releaseDbKeyWriteLock(dbKey);
      throw e;
    }
  },
};

const cache = {};
const cachedStorage = {
  get: async (db, key) => {
    if (typeof key !== "string") {
      return storage.get(db, key);
    }

    const o = cache[`${db} ${key}`];
    if (o) {
      o.count++;
      return o.value;
    }
    const result = storage.get(db, key);
    cache[`${db} ${key}`] = { count: 1, value: result };

    if (Math.random() < 0.01) {
      for (const key in cache) {
        cache[key].count >>= 1;
        if (!cache[key].count) {
          delete cache[key];
        }
      }
    }
    return result;
  },
  put: async (db, key, value, prevVersion, version) => {
    let result = await storage.put(db, key, value, prevVersion, version);
    if (typeof key === "string") delete cache[db + " " + key];
    return result;
  },
  del: async (db, key, prevVersion) => {
    let result = await storage.del(db, key, prevVersion);
    if (typeof key === "string") delete cache[db + " " + key];
    return result;
  },
  since: storage.since,
};

const _db = { id: 0 };
const _token = { id: 1 };
const unchecked = {
  get: async (db, key) => {
    const result = await cachedStorage.get(db.id, key);
    if (!result) {
      throw `key not found: ${key}`;
    }
    return result;
  },
  put: async (db, key, value, prevVersion) => {
    const version = await uniqueVersion();
    await cachedStorage.put(db.id, key, value, prevVersion, version);
    return version;
  },
  since: (db, version) => cachedStorage.since(db.id, version),
  del: (db, key, prevVersion) => cachedStorage.del(db.id, key, prevVersion),
};

unchecked.put(
  _db,
  "_db",
  encode({
    id: 0,
    permissions: { get: ["db-info"], put: [], since: ["db-info"], del: [] },
  })
);
unchecked.put(
  _db,
  "_token",
  encode({
    id: 1,
    permissions: { get: ["token-info"], put: [], since: [], del: [] },
  })
);
unchecked.put(
  _token,
  supertoken,
  encode({
    roles: [
      "superuser-put",
      "superuser-get",
      "superuser-since",
      "superuser-del",
    ],
    expires: "2080-01-01T12:00:00.000Z",
  })
);

const endpoints = {};
for (const endpoint in unchecked) {
  endpoints[endpoint] = async (token, db_name, ...args) => {
    let db = decode((await unchecked.get(_db, db_name))[1]);
    let login = decode((await unchecked.get(_token, token))[1]);
    const permissions = ["superuser-" + endpoint].concat(
      (db && db.permissions && db.permissions[endpoint]) || []
    );
    if (!login || !login.expires || +new Date(login.expires) <= Date.now) {
      login = { roles: [] };
    }
    let roles = ["*"].concat(login.roles);
    if (permissions.some((role) => roles.includes(role))) {
      return unchecked[endpoint](db, ...args);
    }
    throw "permission denied";
  };
}

async function server({ port }) {
  const wss = new WebSocket.Server({
    port: port,
  });
  wss.on("connection", (ws) => {
    let mycrudDBs = [];
    ws.on("message", async (data) => {
      let seq;
      try {
        let result;
        data = decode(data);
        seq = data[0];
        const method = data[1][0];
        const args = data[1].slice(1);
        result = await endpoints[method].apply(null, args);
        let response = encode([seq, result]);
        ws.send(response);
      } catch (e) {
        ws.send(encode([seq, { error: String(e) }]));
      }
    });
  });
}

server({
  port: process.env.MYCRUD_PORT,
});
