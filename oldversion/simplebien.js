(() => {
  const textEncoder = new TextEncoder();
  const textDecoder = new TextDecoder();
  const float = new Float64Array(1);
  const floatBytes = new Uint8Array(float.buffer);

  class SimpleBien {
    constructor(buf) {
      if (buf) {
        if (buf.constructor.name === "Buffer") {
          buf = new Uint8Array(buf);
        }
        if (buf.constructor !== Uint8Array) {
          throw new Error("Parameter not Uint8Array");
        }
        this.buf = buf;
      } else {
        this.buf = new Uint8Array(10);
      }
      this.pos = 0;
    }
    pushByte(byte) {
      if (this.pos === this.buf.length) {
        const t = this.buf;
        this.buf = new Uint8Array((this.buf.length * 1.5 + 1) | 0);
        this.buf.set(t);
      }
      this.buf[this.pos++] = byte;
    }
    encodeVLQ(n) {
      if (n >= 1 << 28) this.pushByte((n / (1 << 28)) | 128);
      if (n >= 1 << 21) this.pushByte(((n >>> 21) & 127) | 128);
      if (n >= 1 << 14) this.pushByte(((n >>> 14) & 127) | 128);
      if (n >= 1 << 7) this.pushByte(((n >>> 7) & 127) | 128);
      this.pushByte(n & 127);
    }
    decodeVLQ() {
      let b = this.buf[this.pos++];
      let n = b & 127;
      while (b & 128) {
        b = this.buf[this.pos++];
        n = n * 128 + (b & 127);
      }
      return n;
    }
    encodeTag(type, i) {
      this.encodeVLQ(i * 8 + type);
    }
    encode(o) {
      if (typeof o === "number") {
        if (o === (o | 0)) {
          if (o >= 0) {
            this.encodeTag(0, o);
          } else {
            this.encodeTag(1, ~o);
          }
        } else {
          this.encodeTag(2, 0);
          float[0] = o;
          for (let i = 0; i < 8; ++i) {
            this.pushByte(floatBytes[i]);
          }
        }
      } else if (o === true) {
        this.encodeTag(2, 1);
      } else if (o === false) {
        this.encodeTag(2, 2);
      } else if (o === undefined || o === null) {
        this.encodeTag(2, 3);
      } else if (Array.isArray(o)) {
        this.encodeTag(4, o.length);
        for (const val of o) {
          this.encode(val);
        }
      } else if (typeof o === "string") {
        const data = textEncoder.encode(o);
        this.encodeTag(5, data.length);
        for (let i = 0; i < data.length; ++i) {
          this.pushByte(data[i]);
        }
      } else if (o instanceof Uint8Array) {
        this.encodeTag(6, o.length);
        for (let i = 0; i < o.length; ++i) {
          this.pushByte(o[i]);
        }
      } else if (typeof o === "object") {
        this.encodeTag(3, Object.keys(o).length);
        for (const key in o) {
          this.encode(key);
          this.encode(o[key]);
        }
      } else {
        throw new Error("unserialisable object: ", o);
      }
    }
    decode() {
      let i = this.decodeVLQ();
      let type = i & 7;
      i = (i / 8) | 0;
      switch (type) {
        case 0:
          return i;
        case 1:
          return ~i;
        case 2:
          switch (i) {
            case 0:
              for (let i = 0; i < 8; ++i) {
                floatBytes[i] = this.buf[this.pos++];
              }
              return float[0];
            case 1:
              return true;
            case 2:
              return false;
            case 3:
              return undefined;
            default:
              throw new Error(
                "invalid constant input at pos: " + (this.pos - 1)
              );
          }
        case 3: {
          let result = {};
          while (i--) {
            let k = this.decode();
            let v = this.decode();
            result[k] = v;
          }
          return result;
        }
        case 4: {
          let result = [];
          while (i--) {
            result.push(this.decode());
          }
          return result;
        }
        case 5:
          this.pos = this.pos + i;
          return textDecoder.decode(this.buf.slice(this.pos - i, this.pos));
        case 6:
          this.pos = this.pos + i;
          return new Uint8Array(this.buf.slice(this.pos - i, this.pos));
        default:
          throw new Error("invalid input at pos: " + (this.pos - 1));
      }
    }
  }

  let instance = new SimpleBien();

  let simplebien = {
    encode: (o) => {
      instance.pos = 0;
      instance.encode(o);
      const result = new Uint8Array(instance.pos);
      result.set(new Uint8Array(instance.buf.buffer, 0, instance.pos));
      return result;
    },
    decode: (buf) => new SimpleBien(buf).decode(),
    SimpleBien,
  };
  if (typeof self !== "undefined") {
    self.mycrudSimplebien = simplebien;
  } else {
    module.exports = simplebien;
  }
})();
