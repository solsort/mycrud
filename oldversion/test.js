let mycrud,
  assert,
  supertoken = "8zrmu8wmn2b",
  port = 8420;
if (typeof process !== "undefined") {
  mycrud = require("./client");
  assert = require("assert");
} else {
  mycrud = self.mycrud;
  assert = (val, message) => {
    if (!val) throw new Error("assert: ", message);
  };
}

function testSimplebien() {
  // TODO test entire 32 bit integer range..
  let data, buf, result;
  data = [
    { hello: "wørld" },
    -100,
    -1,
    0,
    0.1,
    1,
    100,
    10000,
    1000000,
    true,
    false,
    null,
  ];
  buf = mycrud.encode(data);
  result = mycrud.decode(buf);
  assert(JSON.stringify(data) === JSON.stringify(result));
}

async function main() {
  let keys;
  let benchcount = 10000;

  testSimplebien();

  const mc = mycrud.connect(`ws://127.0.0.1:${port}/test.html/ws`);
  const _db = mc.open({ db: "_db", token: supertoken });
  const _token = mc.open({ db: "_token", token: supertoken });

  await _db.put("testdb", {
    id: 2,
    permissions: {
      get: ["test1", "test2", "*"],
      put: ["test1"],
      since: ["test2"],
      del: ["test3"],
    },
  });
  await _token.put("testtoken1", {
    roles: ["test3", "test1"],
    expires: new Date(Date.now() + 5000).toISOString(),
  });
  await _token.put("testtoken2", {
    roles: ["test2"],
    expires: new Date(Date.now() + 5000).toISOString(),
  });

  const db1 = mc.open({ db: "testdb", token: "testtoken1" });
  const db2 = mc.open({ db: "testdb", token: "testtoken2" });
  const dbsuper = mc.open({ db: "testdb", token: supertoken });
  const dbanon = mc.open({ db: "testdb", token: "anonymous" });

  // test user1 can put to database, and gets sensible timestamp
  let k1version = await db1.put("hello", "world");
  assert(
    (Date.now() - 100000) * 1000 < k1version && k1version < Date.now() * 1000
  );

  // test anonymous user kan read database
  assert((await dbanon.get("hello")) === "world");

  /*
  // token1 cannot access since/getKeys
  try {
    await db1.allKeys();
    assert(false);
  } catch(e) {
    assert(e.message === 'permission denied');
  }
  */

  // token2 can fetch all keys;
  keys = await db2.allKeys();
  assert(Array.isArray(keys));
  assert(keys.some(([version, key]) => key === "hello"));

  // wipe the database
  for (const [version, key] of keys) {
    await db1.forceDelete(key, version);
  }
  keys = await db2.allKeys();
  assert(keys.length === 0);

  k1version = await db1.put("hello", "world");
  let withVersion = await db1.getWithVersion("hello");
  assert(withVersion.version === k1version);

  console.log("tests ok, running benchmark");
  console.time("put" + benchcount);
  let ps = [];
  for (let i = 0; i < benchcount; ++i) {
    ps.push(db1.put("hello" + i, Math.random()));
  }
  await Promise.all(ps);
  console.timeEnd("put" + benchcount);
  //console.log(await db1.since(Date.now() - 500), Date.now() - 500);
  console.time("get" + benchcount);
  ps = [];
  for (let i = 0; i < benchcount; ++i) {
    ps.push(db1.get("hello" + i));
  }
  await Promise.all(ps);
  console.timeEnd("get" + benchcount);
  mc.close();

}
main();
