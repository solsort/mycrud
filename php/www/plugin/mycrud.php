<?php
/**
 * Plugin Name: MyCrud
 */

function mycrud_install()
{
  global $wpdb;
  $wpdb->query('CREATE TABLE IF NOT EXISTS ' . $wpdb->prefix . 'mycrud_data (
    db INT NOT NULL,
    k VARCHAR(255) NOT NULL,
    -- The size of the values is limited to 16M 
    -- if v is null, then the value is deleted, but key still in database to propagate deletes when synchronising
    v MEDIUMBLOB, 
    version TIMESTAMP(6) NOT NULL DEFAULT NOW(6) ON UPDATE NOW(6),
    PRIMARY KEY(db, k),
    UNIQUE KEY(db, version)
  );');
  $wpdb->query('CREATE TABLE IF NOT EXISTS ' . $wpdb->prefix . 'mycrud_db (
    id INT NOT NULL AUTO_INCREMENT,
    type TINYINT NOT NULL,
    owner INT NOT NULL, 
    sandbox VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY(id),
    UNIQUE KEY(owner, sandbox, name, type)
  );');
  $result = $wpdb->query('CREATE TABLE IF NOT EXISTS ' . $wpdb->prefix . 'mycrud_token (
    token CHAR(16) NOT NULL,
    url VARCHAR(255) NOT NULL,
    client_id VARCHAR(255) NOT NULL,
    owner INT NOT NULL,
    sandbox VARCHAR(255) NOT NULL,
    permissions TEXT NOT NULL,
    expires TIMESTAMP NOT NULL,
    PRIMARY KEY(token)
  );');
}

class MyCrud {
  public function mycrud_delete_data($dbname, $key) {
    global $wpdb;
    $db = mycrud_db($dbname);
    if (!$db) {
      http_response_code(404);
      exit();
    }
    $meta = mycrud_data_meta($db, $key);
    if (!$meta) {
      http_response_code(404);
      exit();
    }
    $uid = get_current_user_id();
    // TODO more detailed permission checking via data-meta, currently you have to own the entire database
    if ($uid !== (int) $db->owner) {
      http_response_code(403);
      exit();
    }

    if ($db->synchronize_deletes) {
      $wpdb->query($wpdb->prepare(
        "UPDATE {$wpdb->prefix}mycrud_data SET v=NULL WHERE db=%d AND k=%s;",
        $db->id,
        $key
      ));

      // TODO fix potential race condition if two values is inserted at the exact same time.

      $result = $wpdb->get_var($wpdb->prepare(
        "SELECT version FROM {$wpdb->prefix}mycrud_data WHERE db=%d AND k=%s;",
        $db->id,
        $key
      ));
      header('Content-Type: text/plain');
      echo $result;
    } else {
      $result = $wpdb->query($wpdb->prepare(
        "DELETE FROM {$wpdb->prefix}mycrud_data
        WHERE db=%d AND k=%s;",
        $db->id,
        $key
      ));
    }
    exit();
  }
  public function mycrud_get_data($dbname, $key) {
    global $wpdb;
    $db = mycrud_db($dbname);
    if (!$db) {
      http_response_code(404);
      exit();
    }
    $uid = get_current_user_id();
    // TODO more detailed permission checking via data-meta, currently you have to own the entire database
    if ($uid !== (int) $db->owner) {
      http_response_code(403);
      exit();
    }

    $data = $wpdb->get_row($wpdb->prepare(
      "SELECT * FROM {$wpdb->prefix}mycrud_data
      WHERE db=%d AND k=%s;",
      $db->id,
      $key
    ));
    if (!$data) {
      http_response_code(404);
      exit();
    }

    header('Content-Type: ' . $db->mimetype);
    header('ETag: "'.$data->version.'"');

    // TODO handle null with 404
    // || $data->v === NULL
    if ($data->v === null) {
      http_response_code(404);
      exit();
    }
    echo $data->v;
    exit();
  }
  public function mycrud_put_data($dbname, $key) {
    global $wpdb;
    $db = mycrud_db($dbname);
    if (!$db) {
      http_response_code(404);
      exit();
    }
    $meta = mycrud_data_meta($db, $key);
    $value = file_get_contents("php://input");
    $uid = get_current_user_id();
    // TODO more detailed permission checking via data-meta, currently you have to own the entire database
    if ($uid !== (int) $db->owner) {
      http_response_code(404);
      exit();
    }

    $wpdb->query($wpdb->prepare(
      "REPLACE {$wpdb->prefix}mycrud_data SET db=%d, k=%s, v=%s, owner=%d;",
      $db->id,
      $key,
      $value,
      $uid));

    // TODO fix potential race condition if two values is inserted at the exact same time.

    $result = $wpdb->get_var($wpdb->prepare(
      "SELECT version FROM {$wpdb->prefix}mycrud_data WHERE db=%d AND k=%s;",
      $db->id,
      $key
    ));
    header('Content-Type: text/plain');
    echo $result;
    exit();
  }
  public function mycrud_put_db($dbname) {
    global $wpdb;
    $uid = get_current_user_id();
    if (!$uid) {
      http_response_code(403);
      exit();
    }

    $db = mycrud_db($dbname);
    if ($db) {
      // TODO update db definition based on put data, not relevant yet
    } else {
      $result = $wpdb->insert($wpdb->prefix . 'mycrud_dbs', array(
        'name' => $dbname, 
        'owner' => $uid));
      assert($result);
    }
    exit();
  }
  public function mycrud_data_meta($db, $key) {
    global $wpdb;
    return $wpdb->get_row($wpdb->prepare(
      "SELECT owner, version FROM {$wpdb->prefix}mycrud_data WHERE db=%d AND k=%s;",
      $db->id,
      $key
    ));
  }
  public function mycrud_db($dbname) {
    global $wpdb;
    return $wpdb->get_row($wpdb->prepare(
      "SELECT * FROM {$wpdb->prefix}mycrud_dbs WHERE name=%s;",
      $dbname
    ));
  }

  function get($key, $db_type) {
    global $wpdb;
    if($db_type !== 'public' && $db_type !== 'signal') {
      if($this->user !== $this->db->owner || $this->sandbox !== $this->db->sandbox) {
        http_response_code(400);
        echo "forbidden";
        exit();
      }
    }

    $row = $wpdb->get_row($wpdb->prepare(
      "SELECT * FROM {$wpdb->prefix}mycrud_data WHERE db=%d AND k=%s;",
      $this->db->id, $key));
    return $row;
  }

  function put($key, $db_type) {
    global $wpdb;
    if($db_type !== 'inbox' && $db_type !== 'signal') {
      if($this->user !== $this->db->owner || $this->sandbox !== $this->db->sandbox) {
        http_response_code(400);
        echo "forbidden";
        exit();
      }
    }
    $value = file_get_contents("php://input");

    $wpdb->query($wpdb->prepare(
      "REPLACE {$wpdb->prefix}mycrud_data SET db=%d, k=%s, v=%s;",
      $this->db->id,
      $key,
      $value));

    $result = $wpdb->get_var($wpdb->prepare(
      "SELECT version FROM {$wpdb->prefix}mycrud_data WHERE db=%d AND k=%s;",
      $this->db->id,
      $key
    ));
    header('Content-Type: text/plain');
    echo $result;
    exit();
  }

  function since($when, $limit) {
    /*
    global $wpdb;
    if($this->user !== $this->db->owner || $this->sandbox !== $this->db->sandbox) {
      http_response_code(400);
      echo "forbidden";
      exit();
    }
    $rows = $wpdb->get_result(
      $wpdb->prepare("SELECT k, version FROM {$wpdb->prefix}mycrud_data WHERE version>%s ORDER BY VERSION ASC LIMIT %d", $when, $limit);
    // TODO proper result
     */
  }
  function find_db($db_name, $db_type) {
    global $wpdb;
    $db_type_id = array_search($db_type, $this->db_types);
    if($db_type == FALSE) {
      http_response_code(400);
      echo "type not found";
      exit();
    }

    $owner = $this->user;
    $sandbox = $this->sandbox;
    if($db_type == "signal") {
      $owner = 0;
      $sandbox = '';
    } else {
      if(!$this->user) {
        http_response_code(400);
        echo "forbidden";
        exit();
      }
    }

    $row = $wpdb->get_row($wpdb->prepare(
      "SELECT id FROM {$wpdb->prefix}mycrud_db WHERE owner=%d AND sandbox=%s AND name=%s AND type=%d;",
      $owner, $sandbox, $db_name, $db_type_id));

    if(!$row->id && $this->user) {
      $result = $wpdb->insert($wpdb->prefix . 'mycrud_db', array(
        'type' => $db_type_id, 
        'name' => $db_name, 
        'sandbox' => $sandbox, 
        'owner' => $owner));
      $row = $wpdb->get_row($wpdb->prepare(
        "SELECT id FROM {$wpdb->prefix}mycrud_db WHERE owner=%d AND sandbox=%s AND name=%s AND type=%d;",
        $owner, $sandbox, $db_name, $db_type_id));
    }
    echo $row->id;
  }

  var $db_types = ["public", "private", "inbox", "signal"];
  var $user;
  var $sandbox;
  function log() {
    $args = func_get_args();
    error_log("LOG           " . substr(json_encode($args), 1));
  }
  static function template($str, $obj) {
    return preg_replace_callback(
      '/{{([^}]+)}}/',
      function ($match) use ($obj) {
        if (key_exists($match[1], $obj)) {
          return htmlspecialchars($obj[$match[1]]);
        }
        return $match[0];
      },
      $str
    );
  }
  function oauth_authorize() {
    global $wpdb;
    $user = wp_get_current_user();
    $uid = $user->ID;
    if (!key_exists('response_type', $this->query)
      || !key_exists('client_id', $this->query)
      || !key_exists('redirect_uri', $this->query)
      || !key_exists('scope', $this->query)
      || !key_exists('code_challenge', $this->query)
      || !key_exists('code_challenge_method', $this->query)
      || $this->query["response_type"] !== 'code'
      || $this->query["code_challenge_method"] !== 'S256'
      || !preg_match('/^[-a-zA-Z0-9._~]*$/', $this->query['code_challenge'])
    ) {
      http_response_code(400);
      echo "should be /mycrud/v1/oauth/authorize?client_id=com.example.app&redirect_uri=https://example.com/app&response_type=code&scope=SANDBOX_NAME&code_challenge=XXXX&code_challenge_method=S256";
      exit();
    }

    if (!$user->ID) {
      http_response_code(302);
      header('Location: ' . '/wp-login.php?redirect_to=' . urlencode($_SERVER["REQUEST_URI"]));
      exit();
    }

    $token = $this->makeToken();

    $wpdb->insert($wpdb->prefix . 'mycrud_token', array(
      'token' => $token,
      'sandbox' => $this->query['scope'],
      'url' => $this->query['redirect_uri'],
      'client_id' => $this->query['client_id'],
      'owner' => $uid,
      'permissions' => '{"token":true,"challenge":' . json_encode($this->query['code_challenge']) . '}',
      'expires' => date("c", time() + 60 * 60)));

    $state = '';
    if (key_exists('state', $this->query)) {
      $state = "&state=" . $this->query['state'];
    }
    echo $this->template(file_get_contents(__DIR__ . '/oauth_authorize.html'), array(
      "token" => $token,
      "user_firstname" => $user->user_firstname,
      "user_login" => $user->user_login,
      "sandbox" => $this->query['scope'],
      'url' => $this->query['redirect_uri'],
      "target_url" => $this->query['redirect_uri'] . '?code=' . $token . $state));

    exit();
  }
  function makeToken() {
    return strtr(base64_encode(random_bytes(12)), '+/', '-_');
  }
  function oauth_token() {
    global $wpdb;
    parse_str(file_get_contents('php://input'), $query);

    if (!key_exists('grant_type', $query)
      || !key_exists('code', $query)
      || !key_exists('redirect_uri', $query)
      || !key_exists('client_id', $query)
      || !key_exists('code_verifier', $query)
      || $query["grant_type"] !== 'authorization_code'
    ) {
      http_response_code(400);
      echo "invalid request";
      exit();
    }

    $result = $wpdb->get_row($wpdb->prepare(
      "SELECT * FROM {$wpdb->prefix}mycrud_token WHERE token=%s;",
      $query["code"]));
    $permissions = json_decode($result->permissions);

    if( $result->url !== $query["redirect_uri"]
      || strtotime($result->expires) < time()
      || $result->client_id !== $query["client_id"]
      || !$permissions->token
      || !$permissions->challenge
    ) {
      http_response_code(400);
      echo "invalid permissions";
      exit();
    }

    $code_verifier = $query["code_verifier"];
    $challenge = hash('sha256', $code_verifier, true);
    $challenge = strtr(base64_encode($challenge), '+/', '-_'); 
    $challenge = str_replace("=","", $challenge);
    if($permissions->challenge !== $challenge) {
      http_response_code(400);
      echo "challenge failed";
      exit();
    }

    $ttl = 21 * 24 * 60 * 60;

    $token = $this->makeToken();
    // TODO split scope into permissions
    $wpdb->insert($wpdb->prefix . 'mycrud_token', array(
      'token' => $token,
      'url' => $result->url,
      'client_id' => $result->client_id,
      'sandbox' => $result->sandbox,
      'owner' => $result->owner,
      'permissions' => '{"scope":' . json_encode($permissions->scope) . '}',
      'expires' => date("c", time() + $ttl)));


    header('Content-Type: application/json');
    echo '{"access_token":' . json_encode($token) . ',"token_type":"bearer","expires_in": ' . json_encode($ttl) . '}';
    exit();
  }
  public function dispatch($path, $query) {
    global $wpdb;
    $this->query = $query;
    $permissions = array("user" => 0);
    $token = '';
    if (key_exists("token", $query)) {
      $token = $query["token"];
    }

    $this->user = "0";
    $this->sandbox = "";
    if($token) {
      $row = $wpdb->get_row($wpdb->prepare(
        "SELECT * FROM {$wpdb->prefix}mycrud_token
        WHERE token=%s;", $token));
      $this->user = $row->owner;
      $this->sandbox= $row->sandbox;
    }
    if ($path === 'v1/oauth/authorize') {
      $this->oauth_authorize();
    }
    if ($path === 'v1/oauth/token') {
      $this->oauth_token();
    }
    if ($path === 'db') {
      $this->find_db($query["name"], $query["type"]);
      exit();
    }
      $this->log('HERE!', $path);
    $matches = [];
    if (preg_match('#^([0-9]+)/(.*)$#', $path, $matches) === 1) {
      $this->log('HERE2', $path);
      $db = intval($matches[1]);
      $key = $matches[2];
      $this->db = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}mycrud_db WHERE id=%d;", $db));
      $this->get_key = $key;
      $this->put_key = $key;
      $db_type = $this->db_types[$this->db->type];
      $method = $_SERVER['REQUEST_METHOD'];


      if($method === 'PUT' || $method === 'DELETE') {
        if($db_type == "inbox") {
          $key = makeToken();
        }
        if($db_type == "signal") {
          $key = rtrim(strtr(base64_encode(hash('sha256', $key, true)), '+/', '-_'), '=');
        }
      }
      if ($method === 'PUT') {
        $this->put($key, $db_type);
      }
      exit();

      if ($method === 'PUT') {
        mycrud_put_data($key);
      }

      if ($method === 'GET') {
        mycrud_get_data($key);
      }
      if ($method === 'DELETE') {
        mycrud_delete_data($key);
      }
      exit();
    }

    error_log($_SERVER["REQUEST_URI"]);
    exit();
  }
}

function mycrud_url_handler()
{
  global $wpdb;
  if (preg_match('#^/mycrud/#', $_SERVER['REQUEST_URI']) === 1) {
    $parsed = parse_url($_SERVER['REQUEST_URI']);

    $path = substr($parsed["path"], 8); // remove "/mycrud/" prefix

    $query = array();
    if (key_exists("query", $parsed)) {
      parse_str($parsed["query"], $query);
    }

    $mycrud = new MyCrud();
    $mycrud->dispatch($path, $query);
  }
}

add_action('parse_request', 'mycrud_url_handler');
register_activation_hook(__file__, 'mycrud_install');
