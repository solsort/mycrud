import axios from 'axios';
import qs from 'qs';
import {AbstractLevelDOWN} from 'abstract-leveldown';

class MyCrud extends AbstractLevelDOWN {
  constructor(url) {
    super();
    this.url = url;
  }
  async _open(options, callback) {
    try {
      // create db if not exists
      await axios.put('/mycrud/newdb')
    } catch(e) {
    }
    callback(null, this);
  }
  async _get(key, options, callback) {
    try {
      const result = await axios.get(this.keyUrl(key))
      callback(null, result.data);
    } catch(e) {
      if(e.request.status === 404) {
        callback(new Error('NotFound'));
      } else {
        callback(e);
      }
    }
  }
  keyUrl(key) {
    return this.url + '/' + encodeURIComponent(key)
  }
  async _put(key, value, options, callback) {
    try {
      const result = await axios.put(this.keyUrl(key), value)
      callback(null);
    } catch(e) {
      callback(e);
    }
  }
  async _del(key, options, callback) {
    try {
      const result = await axios.put(this.keyUrl(key))
      callback(null);
    } catch(e) {
      callback(e);
    }
  }
}

const btoa_url = s => btoa(s).replace(/[+]/g, '-').replace(/\//g, '_').replace(/=*$/, '');
const u8arr_to_str = a =>  String.fromCharCode.apply(null, new Uint8Array(a));
const str_to_u8arr = s =>  Uint8Array.from(s.split('').map(s => s.charCodeAt(0)));

async function mycrudToken(client_id, scope) {
  if(localStorage.getItem('mycrudCodeVerifier') && window.location.search.startsWith('?code=')) {
    const codeVerifier = localStorage.getItem('mycrudCodeVerifier')
    console.log('codeVerifier', codeVerifier);
    localStorage.removeItem('mycrudCodeVerifier');
    const query = {
      grant_type: 'authorization_code',
      code: window.location.search.slice(6),
      redirect_uri: window.location.href.replace(/[?#].*/,''),
      client_id: client_id,
      code_verifier: codeVerifier
    };
    const result = await axios.post('/mycrud/v1/oauth/token', qs.stringify(query));
    console.log(result.data);
    return result.data.access_token;
  } else {
    const codeVerifier = btoa_url(u8arr_to_str(window.crypto.getRandomValues(new Uint8Array(33))));
    const challengeBytes =  await window.crypto.subtle.digest('SHA-256', str_to_u8arr(codeVerifier));
    const challenge =  btoa_url(u8arr_to_str(challengeBytes));
    console.log('xxx', codeVerifier, challenge);
    localStorage.setItem('mycrudCodeVerifier', codeVerifier)
    const query = {
      client_id,
      redirect_uri: window.location.href.replace(/[?#].*/,''),
      response_type: 'code',
      scope,
      code_challenge: challenge,
      code_challenge_method: 'S256'
    }
    //console.log(codeVerifier, query, qs.stringify(query));
    window.location.href = '/mycrud/v1/oauth/authorize?' + qs.stringify(query);
      
      /*

      http://localhost:3000/mycrud/v1/oauth/authorize?client_id=com.example.app&redirect_uri=https://example.com/app&response_type=code&scope=mycrud:example.*&code_challenge=XXXX&code_challenge_method=S256
      response_type=code&scope=mycrud:example.*&code_challenge=XXXX&code_challenge_method=S256
    
    }


  */
  }
}
async function main() {
  const token =  await mycrudToken('test', 'test sandbox');
  console.log('token',token);

  if(token) {
    const db = (await axios.get(`/mycrud/db?name=hello&type=public&token=${token}`)).data;
    const result = await axios.put(`/mycrud/${db}/hello?token=${token}`, "hello world");
    console.log(result.data);
  }
}
main();
