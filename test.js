const assert = require("assert");
const fetch = require("node-fetch");
const baseUrl = `http://localhost:${process.env.MYCRUD_PORT || 8000}/mycrud/`;
const adminToken = process.env.MYCRUD_TOKEN;
let token = adminToken;
let log = true;
const testUTF = true;

async function test() {
  let o;
  for (const db in dbs) {
    await put("_db", db, JSON.stringify(dbs[db]));
  }
  for (const token in tokens) {
    await put("_token", token, JSON.stringify(tokens[token]));
  }
  await put("_token", "testTokenInvalid", "NOT JSON ERROR");
  token = "testToken1";

  // prepare TTL-test
  await put("testTTL", "should-be-deleted", "ttl-test");

  //
  // TEST: put
  //
  // 404 database
  await put("this database does not exist", "key", "value");
  // write permission-denied
  await put("test2", "key", "access denied");
  // forceVersion permission-denied
  await put("test1", "key", "access denied", {
    forceVersion: new Date().toISOString(),
  });
  // vanilla
  let o1 = await put("test1", "some-key", "this should work");
  // prevVersion=null
  let o2 = await put("test1", "some-other-key", "this should also work", {
    prevVersion: "null",
  });
  // prevVersion conflict
  await put("test1", "some-key", "should fail with wrong prevVersion", {
    prevVersion: o2.v,
  });
  // prevVersion
  await put("test1", "some-key", "should work with correct prevVersion", {
    prevVersion: o1.v,
  });
  // prevVersion=null conflict
  await put("test1", "some-key", "should fail already exists", {
    prevVersion: null,
  });
  // forceVersion
  token = "testToken3";
  await put("test1", "some-key", "update should work with new timestamp", {
    forceVersion: "2020-01-01T00:00:00.000000",
  });
  // extra put

  //
  // TEST get
  //
  // read permission-denied
  await get("test1", "some-key");
  // vanilla
  token = "testToken1";
  await get("test1", "some-key");
  // 404 value
  await get("test1", "this-key-dows-not-exist");
  // 404 database
  await get("this database does not exist", "key");

  // Test unicode + several writes
  let ks = [];
  for (let i = 0; i < 16; ++i) {
    await put("test1", "key" + i, `${i} ${i * i} ${i * i * i} ${1 / i}`);
    let s = "";
    for (let j = 0; j < 16; ++j) {
      s += testUTF
        ? String.fromCharCode(i * 16 + j)
        : (i * 16 + j).toString(16);
    }
    await put("test2", encodeURIComponent(s), s);
    let o = await get("test2", encodeURIComponent(s));
    console.log(JSON.stringify(o.v), JSON.stringify(s));
    assert.equal(JSON.stringify(o.v), JSON.stringify(s));
    ks.push(encodeURIComponent(s));
  }
  await put("test2", "key", "value");

  //
  // TEST: delete
  //
  // permission-denied
  await del("test2", ks[0]);
  // vanilla
  token = "testToken2";
  await del("test2", ks[0]);
  // wrong prevVersion
  await del("test1", "key1", { prevVersion: o1.v });
  await del("test2", ks[1], { prevVersion: o1.v });
  // forceVersion + prevVersion
  o1 = await get("test1", "key1");
  o2 = await get("test2", ks[1]);
  await del("test1", "key1", { prevVersion: o1.ts });
  await del("test2", ks[1], { prevVersion: o2.ts });
  // forceVersion
  await del("test1", "key2", { forceVersion: "2020-01-01T00:00:00.000000" });
  await del("test2", ks[2], { forceVersion: "2020-01-01T00:00:00.000000" });

  token = "testToken1";
  for (let i = 0; i < 100; ++i) {
    await put("test1", i + 1000, `${i} ${i * i} ${i * i * i} ${1 / i}`);
  }
  //
  // TEST: find
  //
  // permission-denied
  await find("testTTL", { since: "0" });
  // limit
  o = await find("test1", { since: "0", limit: 7 });
  assert.equal(JSON.parse(o.v).length, 7);
  // gte
  o = await find("test1", { gte: 1003, limit: 3 });

  // lt, gt, reverse
  o = await find("test1", { lt: 1007, gt: 1000, reverse: true });
  // lte
  o = await find("test1", { lte: 1007 });
  // since
  o = await find("test1", {
    limit: 10,
    since: new Date(Date.now() - 10000).toISOString(),
  });
  // reverse
  // wait
  o = await find("test1", { since: "0", wait: 1, limit: 1 });
  p = find("test1", { lte: "0", wait: 1, limit: 1 });
  await sleep(100);
  put("test1", "0", "waiting for it");
  await p;

  // since+lt error
  await find("test1", { lt: "0", since: "0" });

  // special characters escaped
  await find("test2", { since: "0" });

  //
  // TEST: misc
  //
  // non-json token definition
  token = "testTokenInvalid";
  await find("testTTL", { since: "0" });
  token = "testToken1";

  // cors
  o = await fetch(baseUrl + "test2/key", {
    headers: { origin: "example.com" },
  });
  assert.equal(await o.text(), "value");
  let h = o.headers;
  assert.equal(h.get("access-control-allow-origin"), "example.com");
  assert.equal(h.get("access-control-allow-methods"), "GET,POST,PUT,DELETE");
  assert.equal(h.get("access-control-allow-credentials"), "true");
  assert.equal(
    h.get("access-control-allow-headers"),
    "Authorization, Content-Type"
  );
  // options
  o = await fetch(baseUrl + "test2/key", {
    method: "OPTIONS",
    headers: { origin: "example.com" },
  });
  assert.equal(await o.text(), "");
  // +16MB
  let bigfile = "";
  for (let i = 0; i < 16000000; ++i) {
    bigfile += "x";
  }

  await put("test1", "bigfile", bigfile);
  for (let i = 0; i < 1000000; ++i) {
    bigfile += "x";
  }
  await put("test1", "bigfile", bigfile);

  //await get("testTTL", "invalid key to make sure we trigger cleanup code");
  // with empty key
  await put("test1", "", "has empty key");
  await get("test1", "");
  // with empty value
  await put("test1", "empty value", "");
  await get("test1", "empty value");
  // key maxlength handling/error
  let longkey = "";
  for (let i = 0; i < 256; ++i) {
    longkey += "x";
  }
  await put("test1", longkey, "too long key");
  await put("test1", longkey.slice(1), "ok key");
  // TTL
  await get("testTTL", "should-be-deleted");
  // deleteTTL
  await find("test1", { deleted: true, since: "0" });
  await find("test2", { deleted: true, since: "0" });

  o = await fetch(baseUrl + "_db/testTTL");
  o = await o.text();
  assert.equal(JSON.parse(o).notes, "Blåbærgrød –");
  assert.equal(o.indexOf("å"), -1);
  console.log(o);
  //console.log(JSON.stringify(JSON.parse(o.v)));
}

async function initialise() {
  // if database is uninitialised, wait for it to be initialised
  await fetch(baseUrl + "_db/_db");
  await sleep(100);
}

let dbs = {
  testTTL: {
    permissions: {
      create: ["test-1"],
      read: ["test-1"],
      update: ["test-1"],
      delete: ["test-1"],
      forceVersion: ["test-1"],
    },
    TTL: 1,
    notes: "Blåbærgrød –",
  },
  test1: {
    permissions: {
      create: ["test-1", "test-3"],
      read: ["test-1", "test-2"],
      update: ["test-1", "test-3"],
      delete: ["test-1", "test-2"],
      find: ["test-1"],
      forceVersion: ["test-3", "test-2"],
    },
    TTL: 600,
  },
  test2: {
    permissions: {
      create: ["test-1"],
      read: ["*"],
      update: ["test-1"],
      delete: ["test-2"],
      find: ["test-2", "test-1"],
      forceVersion: ["test-2"],
    },
    TTL: 600,
    deletedTTL: 0,
  },
};
let expiry = new Date(Date.now() + 600 * 1000).toISOString();
let tokens = {
  testToken1: {
    roles: ["test-1"],
    expires: expiry,
  },
  testToken2: {
    roles: ["test-2"],
    expires: expiry,
  },
  testToken3: {
    roles: ["test-3"],
    expires: expiry,
  },
};
async function main() {
  await initialise();

  startTest();
  await test();
  endTest();
}
main();

function startTest() {
  console.time("test");
}
function endTest() {
  console.timeEnd("test");
}

async function http(method, url, opt) {
  opt = opt || {};
  let req = JSON.stringify([method, url.slice(baseUrl.length), opt, token]);
  req = req.replace(adminToken, "ADMIN_TOKEN");

  if (log) {
    console.log(req.slice(0, 210));
  }

  let { body } = opt;
  let args = {
    method,
    headers: { Authorization: `Bearer ${token}` },
  };
  if (opt.body) {
    args.body = opt.body;
    args.headers["Content-Type"] = "text/plain";
  }

  let result = await fetch(url, args);
  let text = await result.text();

  result = {
    status: result.status,
    "content-type": result.headers.get("content-type"),
    etag: result.headers.get("etag"),
    text,
  };

  res = JSON.stringify(result);
  res = res.replace(adminToken, "ADMIN_TOKEN");
  res = res.replace(
    /\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d.\d\d\d\d\d\d/g,
    "YYYY-MM-DDThh:mm:ss.ssssss"
  );

  if (log) {
    console.log(
      "=> ",
      res
        .slice(0, 207)
        .replace(
          /[^ -~]/g,
          (s) => "\\u" + (256 * 256 + s.charCodeAt(0)).toString(16).slice(1)
        )
    );
  }

  // TODO snapshot

  return result;
}
function qs(o) {
  let result = [];
  for (let k in o) {
    result.push(`${k}=${o[k]}`);
  }
  return result.join("&");
}
async function get(db, key) {
  let result = await http("GET", `${baseUrl}${db}/${key}`);
  return {
    status: result.status,
    v: result.text,
    ts: result.etag && result.etag.replace(/"/g, ""),
  };
}
async function put(db, key, value, opt) {
  opt = opt || {};
  let result = await http("PUT", `${baseUrl}${db}/${key}?${qs(opt)}`, {
    body: value,
  });
  return {
    status: result.status,
    v: result.text,
  };
}
async function find(db, opt) {
  opt = opt || {};
  let result = await http("GET", `${baseUrl}${db}?${qs(opt)}`);
  return {
    status: result.status,
    v: result.text,
  };
}
async function del(db, key, opt) {
  opt = opt || {};
  let result = await http("DELETE", `${baseUrl}${db}/${key}?${qs(opt)}`);
  return {
    status: result.status,
    v: result.text,
  };
}
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
